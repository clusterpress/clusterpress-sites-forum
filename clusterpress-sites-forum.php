<?php
/**
 * Plugin Name: ClusterPress Sites Forum
 * Plugin URI: https://cluster.press
 * Description: Ce cluster requiert bbPress et remplace le profil bbPress par une nouvelle section dans le profil ClusterPress. Si la configuration de WordPress est Multisite, les pages de découverte des sites intégreront un forum dédié.
 * Version: 1.1.0-alpha
 * License: GPLv2 or later (license.txt)
 * Author: imath
 * Author URI: https://imathi.eu/
 * Text Domain: clusterpress-sites-forum
 * Domain Path: /languages/
 * Network: True
 * Default Language: fr_FR
 * cluster_id: forums
 * cluster_name: Forums de Site
 * cluster_icon: img/sites-forum.png
 * cluster_requires: 4.7
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Only Load the Forums Cluster files and classes if on the main site.
 *
 * @since  1.0.0
 *
 * @param  string $dir     Path to the Cluster's direcrtory.
 * @param  string $cluster ID of the Cluster being loaded.
 * @return string          Path to the Forums Cluster directory.
 */
function cpsf_set_forum_cluster_dir( $dir = '', $cluster = '' ) {
	if ( empty( $cluster ) || 'forums' !== $cluster || ! cp_is_main_site() ) {
		return $dir;
	}

	return plugin_dir_path( __FILE__ );
}
add_filter( 'cp_custom_cluster_includes_dir', 'cpsf_set_forum_cluster_dir',  10, 2 );

/**
 * Edit the Cluster's object to add a bbPress property
 *
 * This property is used to check bbPress is available and loaded.
 *
 * @since  1.0.0
 */
function cpsf_bbpress_is_loaded( &$cluster ) {
	if ( empty( $cluster->id ) || 'forums' !== $cluster->id ) {
		return;
	}

	$cluster->bbpress_loaded = true;
}

/**
 * Wait for bbPress and add a specific global to the Cluster.
 *
 * Also, add the plugin's cluster name strings so that it can be translated.
 *
 * @since  1.0.0
 */
function clusterpress_sites_forum() {
	// Used in the plugin's header.
	$cluster_name = __( 'Forums de Site', 'clusterpress-sites-forum' );

	add_action( 'cp_forums_cluster_globals', 'cpsf_bbpress_is_loaded', 10, 1 );
}
add_action( 'bbp_loaded', 'clusterpress_sites_forum' );
