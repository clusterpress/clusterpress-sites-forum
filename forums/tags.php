<?php
/**
 * User profile Template Tags
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums
 * @subpackage tags
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Add a user feedback to display.
 *
 * @since  1.0.0
 *
 * @param  string $message The message to display.
 * @param  string $object  The name of the object it relies to.
 * @param  string $type    The type of feedback (updated, error, info).
 */
function cpsf_add_screen_feedback( $message = '', $object = '', $type = 'info' ) {
	if ( empty( $message ) || empty( $object ) ) {
		return;
	}

	if ( empty( $type ) ) {
		$type = 'info';
	}

	clusterpress()->feedbacks->add_feedback(
		sprintf( 'forum[%s]', $object ),
		/**
		 * Filter here to edit the feedback message to display.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $message The message to display.
		 * @param  string $object  The name of the object it relies to.
		 */
		apply_filters( 'cpsf_add_screen_feedback', $message, $object ),
		$type
	);
}

/**
 * Display a user feedback.
 *
 * @since  1.0.0
 *
 * @param  string $message The message to display.
 * @param  string $object  The name of the object it relies to.
 * @param  string $type    The type of feedback (updated, error, info).
 * @return string          HTML Output.
 */
function cpsf_display_feedback( $message = '', $object = '', $type = '' ) {
	$feedback = cpsf_add_screen_feedback( $message, $object, $type );

	cp_get_template_part( 'assets/feedbacks' );
}

/**
 * Display bbPress specific notices.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_template_notices() {
	// Bail if no notices or errors
	if ( ! bbp_has_errors() ) {
		return;
	}

	// Define local variable(s)
	$errors = $messages = array();

	// Get bbPress
	$bbp = bbpress();

	// Loop through notices
	foreach ( $bbp->errors->get_error_codes() as $code ) {

		// Get notice severity
		$severity = $bbp->errors->get_error_data( $code );

		// Loop through notices and separate errors from messages
		foreach ( $bbp->errors->get_error_messages( $code ) as $error ) {
			if ( 'message' === $severity ) {
				$messages[] = $error;
			} else {
				$errors[]   = $error;
			}
		}
	}

	$object = cp_current_action();
	if ( cp_is_site_manage() ) {
		$object = cp_current_sub_action();
	}

	// Display errors first...
	if ( ! empty( $errors ) ) {
		cpsf_display_feedback( sprintf( '<p>%s</p>',  join( "</p>\n<p>", $errors ) ), $object, 'error' );
	}

	// Then display warnings..
	if ( ! empty( $messages ) ) {
		cpsf_display_feedback( sprintf( '<p>%s</p>',  join( "</p>\n<p>", $messages ) ), $object, 'info' );
	}
}

/**
 * We need to use a specific tag for forum subscriptions
 *
 * @since  1.0.0
 *
 * @param   int $user_id The displayed User ID.
 * @return  bool         True if the user has forum subscriptions, false otherwise.
 */
function cpsf_get_user_forum_subscriptions( $user_id = 0 ) {
	add_filter( 'bbp_is_subscriptions', '__return_true' );

	$has_forum_subscriptions = bbp_get_user_forum_subscriptions( $user_id );

	remove_filter( 'bbp_is_subscriptions', '__return_true' );

	return $has_forum_subscriptions;
}

/**
 * Temporarly override the bbPress single user replies conditional tag.
 *
 * @since  1.0.0
 *
 * @return bool True.
 */
function cpsf_set_is_user_replies() {
	add_filter( 'bbp_is_single_user_replies', '__return_true' );
}

/**
 * Remove the temporary override on the bbPress single user replies conditional tag.
 *
 * @since  1.0.0
 */
function cpsf_unset_is_user_replies() {
	remove_filter( 'bbp_is_single_user_replies', '__return_true' );
}

/**
 * Get the search terms using a specific query var.
 *
 * @since  1.0.0
 *
 * @return string The search terms.
 */
function cpsf_get_search_terms() {
	if ( empty( $_GET['ts'] ) ) {
		return '';
	}

	return bbp_get_search_terms( $_GET['ts'] );
}

/**
 * Displays the bbPress user's screen according to the context.
 *
 * @since  1.0.0
 *
 * @param  int    $user_id The User ID of the user to display bbPress screens for.
 * @return string HTML Output.
 */
function cpsf_display_user_screens( $user_id = 0 ) {
	if ( ! cp_is_user() ) {
		return;
	}
	?>
	<div id="bbpress-forums">

		<div class="bbp-user-section">

		<?php if ( cp_is_current_sub_action( cpsf_get_user_topics_archive_slug() ) ) : ?>

			<?php if ( bbp_get_user_topics_started( $user_id ) ) : ?>

				<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

				<?php bbp_get_template_part( 'loop',       'topics' ); ?>

				<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

			<?php else :

				cpsf_display_feedback(
					esc_html__( 'Cet utilisateur n\'a pas encore démarré de sujets de forum.', 'clusterpress-sites-forum' ),
					bbp_get_topic_archive_slug()
				);

			endif; ?>

		<?php elseif ( cp_is_current_sub_action( cpsf_get_user_replies_archive_slug() ) ) : ?>

			<?php if ( bbp_get_user_replies_created( $user_id ) ) : ?>

				<?php cpsf_set_is_user_replies(); ?>

				<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

				<?php bbp_get_template_part( 'loop',       'replies' ); ?>

				<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

				<?php cpsf_unset_is_user_replies(); ?>

			<?php else :

				cpsf_display_feedback(
					esc_html__( 'Cet utilisateur n\'a pas encore apporté de réponses à un sujet de forum.', 'clusterpress-sites-forum' ),
					bbp_get_reply_archive_slug()
				);

			endif; ?>

		<?php elseif ( cp_is_current_sub_action( cpsf_get_user_favorites_archive_slug() ) ) : ?>

			<?php if ( bbp_get_user_favorites( $user_id ) ) : ?>

				<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

				<?php bbp_get_template_part( 'loop',       'topics' ); ?>

				<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

			<?php else :

				cpsf_display_feedback(
					esc_html__( 'Cet utilisateur n\'a pas encore mis en favoris des contenus du forum.', 'clusterpress-sites-forum' ),
					bbp_get_user_favorites_slug()
				);

			endif; ?>

		<?php elseif ( cp_is_current_sub_action( cpsf_get_user_subscriptions_archive_slug() ) ) : ?>

			<div class="subscribed-forums">

				<h2><?php esc_html_e( 'Suivis des forums', 'clusterpress-sites-forum' ); ?></h2>

				<?php if ( cpsf_get_user_forum_subscriptions( $user_id ) ) : ?>

					<?php bbp_get_template_part( 'loop', 'forums' ); ?>

				<?php else :

					cpsf_display_feedback(
						esc_html__( 'Cet utilisateur n\'est abonné à aucun forum.', 'clusterpress-sites-forum' ),
						'forums-' . bbp_get_user_subscriptions_slug()
					);

				endif; ?>

			</div>

			<div class="subscribed-topics">

				<h2><?php esc_html_e( 'Suivis des sujets de forum', 'clusterpress-sites-forum' ); ?></h2>

				<?php if ( bbp_get_user_topic_subscriptions( $user_id ) ) : ?>

					<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

					<?php bbp_get_template_part( 'loop',       'topics' ); ?>

					<?php bbp_get_template_part( 'pagination', 'topics' ); ?>

				<?php else :

					cpsf_display_feedback(
						esc_html__( 'Cet utilisateur n\'est abonné à aucun sujet de forum.', 'clusterpress-sites-forum' ),
						'topics-' . bbp_get_user_subscriptions_slug()
					);

				endif; ?>

			</div>

		<?php endif ;?>

		</div> <!-- .bbp-user-section -->

	</div><!-- #bbpress-forums -->
	<?php
}
