<?php
/**
 * The Cluster's class.
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums\classes
 * @subpackage CP Forums Cluster
 *
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Sites Forum Cluster class.
 */
class CP_Forums_Cluster extends CP_Cluster {

	/**
	 * Constructor.
	 *
	 * @since  1.0.0
	 *
	 * @param array $args The Cluster's parameters.
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Instantiate the Cluster if not set yet.
	 *
	 * @since  1.0.0
	 *
	 * @return CP_Forums_Cluster This class.
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->forums ) ) {
			$cp->forums = new self( array(
				'id'            => 'forums',
				'name'          => __( 'Forums', 'clusterpress-sites-forum' ),
				'absdir'        => plugin_dir_path( dirname( __FILE__ ) ),
			) );

			$cp->forums->version   = '1.1.0-alpha';
			$plugin_basename       = dirname( dirname( __FILE__ ) );
			$cp->forums->path_data = array(
				'dir' => plugin_dir_path( $plugin_basename ),
				'url' => plugin_dir_url ( $plugin_basename ),
			);
		}

		return $cp->forums;
	}

	/**
	 * Includes the needed files.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files/classes to include
	 */
	public function load_cluster( $files = array() ) {
		if ( empty( $this->bbpress_loaded ) ) {
			return;
		}

		$files['files'] = array(
			'functions',
			'tags',
			'actions',
			'filters',
		);

		if ( is_multisite() && cp_cluster_is_enabled( 'site' ) ) {
			$files['files'] = array_merge( $files['files'], array(
				'site/functions',
				'site/tags',
				'site/filters',
				'site/actions',
			) );
		}

		if ( cp_cluster_is_enabled( 'interactions' ) && get_network_option( 0, 'clusterpress_sites_forum_mentions', true ) ) {
			$files['files'][] = 'mentions';
		}

		parent::load_cluster( $files );
	}

	/**
	 * Sets specific hook to the Cluster.
	 *
	 * @since  1.0.0
	 */
	public function set_cluster_hooks() {
		// Load languages
		add_action( 'cp_include_clusters', array( $this, 'load_languages' ) );

		$this->uninstall = get_site_transient( '_clusterpress_sites_forum_uninstall' );

		if ( empty( $this->bbpress_loaded ) || ! empty( $this->uninstall ) ) {
			add_action( 'cp_admin_notices', array( $this, 'admin_notice' ) );
			return;
		}

		// Enqueue style and scripts.
		add_action( 'cp_enqueue_cssjs_clusterpress', array( $this, 'enqueue_cssjs' ), 10, 1 );

		// Eventually upgrade the plugin
		add_action( 'cp_admin_init', array( $this, 'plugin_upgrade' ), 999 );

		// Eventually save a transient to ask the user to deactivate/reactivate the Cluster.
		add_action( 'cp_set_clusters', array( $this, 'please_deactivate' ) );
	}

	/**
	 * Displays a notice if bbPress is not active on the main site.
	 *
	 * @since  1.0.0
	 *
	 * @return HTML Output.
	 */
	public function admin_notice() {
		if ( ! empty( $this->uninstall ) ) {
			$notices = array(
				esc_html__( 'ClusterPress Sites Forum doit être activé après le Cluster des sites.', 'clusterpress-sites-forum' ),
				esc_html__( 'Merci de désactiver ce ClusterPress Sites Forum et de le réactiver, maintenant que celui des sites l\'est.', 'clusterpress-sites-forum' ),
			);

			delete_site_transient( '_clusterpress_sites_forum_uninstall' );
		} else {
			$notices = array(
				esc_html__( 'ClusterPress Sites Forum nécessite que l\'extension bbPress soit activée sur le site principal.', 'clusterpress-sites-forum' ),
				esc_html__( 'Merci d\'activer bbPress ou de désactiver ce Cluster.', 'clusterpress-sites-forum' ),
			);
		}

		printf( '<div class="error notice is-dismissible">%s</div>', '<p>' . join( '</p><p>', $notices ) . '</p>' );
	}

	/**
	 * Conditionaly enqueues the Cluster's style.
	 *
	 * @since 1.0.0
	 *
	 * @param  string $min The minified suffix.
	 */
	public function enqueue_cssjs( $min = '' ) {
		if ( ! cpsf_is_site_forum() && ! cpsf_is_manage_site_forum()
			&& ! cpsf_is_user_forum() && ! cpsf_is_manage_user_forum() ) {
			return;
		}

		add_filter( 'cp_template_stack', array( $this, 'add_dir_to_template_stack' ), 10, 1 );

		wp_enqueue_style( 'clusterpress-sites-forum', cp_get_template_asset( 'css/bbpress' . $min, 'css', $this->path_data ), array( 'dashicons' ), $this->version );

		remove_filter( 'cp_template_stack', array( $this, 'add_dir_to_template_stack' ), 10, 1 );
	}

	/**
	 * Temporarly add the Cluster's tempate directory to ClusterPress's templates stack.
	 *
	 * @since  1.0.0
	 *
	 * @param  array $stack The ClusterPress templates stack.
	 * @return array        The ClusterPress templates stack.
	 */
	public function add_dir_to_template_stack( $stack = array() ) {
		// Make sure to put it at the last position
		$index = max( array_keys( $stack ) ) + 1;

		$stack[ $index ] = trailingslashit( $this->path_data['dir'] ) . 'templates';

		return $stack;
	}

	/**
	 * Parse the query to set site's forum objects.
	 *
	 * NB: This is only happening on multisite configs and if the site's Cluster is enabled.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Query $q The WP Query object.
	 */
	public function parse_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) || empty( $this->bbpress_loaded ) || ! cp_cluster_is_enabled( 'site' ) || ! is_multisite() ) {
			return;
		}

		if ( ! cpsf_is_site_forum() && ! cpsf_is_manage_site_forum() ) {
			return;
		}

		// Get the main instance of ClusterPress
		$cp = clusterpress();

		// Get the forum ID of the displayed site
		$forum_id = cpsf_get_site_forum_id();

		if ( ! empty( $forum_id ) ) {
			$forum = get_posts( array(
				'p'           => $forum_id,
				'post_parent' => cpsf_get_site_forum_parent_id(),
				'post_type'   => bbp_get_forum_post_type(),
			) );

			if ( empty( $forum ) || ! is_array( $forum ) ) {
				$q->set_404();
				return;
			}

			$forum = bbp_get_forum( reset( $forum ) );

			if ( empty( $forum->ID ) ) {
				$q->set_404();
				return;
			}

			// Globalize the current topic.
			$cp->forums->forum = $forum;

			// We're on a single forum till a topic is displayed
			$cp->forums->is_single_forum = true;

		// We need to be able to create a forum!!
		} elseif ( ! cpsf_is_manage_site_forum() ) {
			$q->set_404();
			return;
		}

		// Look for a topic slug
		$topic_slug = $q->get( 'cpsf_topic' );

		// We have one, then it's no more a single forum.
		if ( $topic_slug ) {
			$cp->forums->is_single_forum = false;

			$query_topic_args = array(
				'name'        => $topic_slug,
				'post_type'   => bbp_get_topic_post_type(),
				'post_status' => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
			);

			if ( is_super_admin() || false !== array_search( get_current_user_id(), cp_displayed_site()->admins ) ) {
				$query_topic_args['post_status'] = array_merge( $query_topic_args['post_status'], array(
					bbp_get_spam_status_id(),
					bbp_get_trash_status_id(),
				) );
			}

			$topic = get_posts( $query_topic_args );

			if ( empty( $topic ) || ! is_array( $topic ) ) {
				$q->set_404();
				return;
			}

			$topic = bbp_get_topic( reset( $topic ) );

			if ( empty( $topic->ID ) ) {
				$q->set_404();
				return;
			}

			// Globalize the current topic.
			$cp->forums->topic = $topic;

			// Set the ClusterPress sub action
			$cp->cluster->sub_action = cpsf_get_site_forum_topic_slug();

			// Set the is single topic global
			$cp->forums->is_single_topic = true;

			$topic_edit = $q->get( 'cpsf_edit' );

			if ( $topic_edit ) {
				$cp->forums->is_topic_edit = true;
			}
		}

		// Look for a topic slug
		$tag_slug = $q->get( 'cpsf_topic_tag' );

		if ( $tag_slug ) {
			$cp->forums->is_topic_tag   = $tag_slug;
			$cp->forums->topic_tag_term = get_term_by( 'slug', $tag_slug, bbp_get_topic_tag_tax_id() );

			if ( empty( $cp->forums->topic_tag_term->term_id ) ) {
				$q->set_404();
				return;
			}

			$tag_edit = $q->get( 'cpsf_edit' );

			if ( $tag_edit ) {
				$cp->forums->is_single_forum   = false;
				$cp->forums->is_topic_tag_edit = true;
			}
		}

		// Look for a reply ID
		$reply_id = $q->get( 'cpsf_reply' );

		if ( $reply_id ) {
			$cp->forums->is_single_forum = false;

			$reply = bbp_get_reply( $reply_id );

			if ( empty( $reply->ID ) ) {
				$q->set_404();
				return;
			}

			// Globalize the current topic.
			$cp->forums->reply = $reply;

			// Set the ClusterPress sub action
			$cp->cluster->sub_action = cpsf_get_site_forum_reply_slug();

			// Set the is single reply global
			$cp->forums->is_single_reply = true;

			$reply_edit = $q->get( 'cpsf_edit' );

			if ( $reply_edit ) {
				$cp->forums->is_reply_edit = true;
			}
		}

		parent::parse_query( $q );
	}

	/**
	 * Register the rewrite tags for the Cluster.
	 *
	 * NB: This is only happening on multisite configs and if the site's Cluster is enabled.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $tags The list of rewrite tags.
	 * @return array        The list of rewrite tags.
	 */
	public function rewrite_tags( $tags = array() ) {
		if ( empty( $this->bbpress_loaded ) || ! cp_cluster_is_enabled( 'site' ) || ! is_multisite() ) {
			parent::rewrite_tags( array() );
			return;
		}

		parent::rewrite_tags( array(
			array(
				'tag'   => '%cpsf_topic%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%cpsf_reply%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%cpsf_topic_tag%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%cpsf_edit%',
				'regex' => '([1]{1,})',
			),
		) );
	}

	/**
	 * Register the rewrite rules for the Cluster.
	 *
	 * NB: This is only happening on multisite configs and if the site's Cluster is enabled.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $rules The list of rewrite rules.
	 * @return array         The list of rewrite rules.
	 */
	public function rewrite_rules( $rules = array() ) {
		if ( empty( $this->bbpress_loaded ) || ! cp_cluster_is_enabled( 'site' ) || ! is_multisite() ) {
			parent::rewrite_rules( array() );
			return;
		}

		$forum_regex = trailingslashit( cp_get_root_slug() ) . cp_site_get_slug() . '/([^/]+)/' . cpsf_get_site_topics_archive_slug();
		$page_slug   = cp_get_paged_slug();
		$topic_slug  = cpsf_get_site_forum_topic_slug();
		$tag_slug    = cpsf_get_site_forum_tag_slug();
		$page_id     = 'paged';
		$topic_id    = 'cpsf_topic';
		$tag_id      = 'cpsf_topic_tag';
		$edit_slug   = bbp_get_edit_rewrite_id();
		$edit_id     = 'cpsf_edit';
		$reply_slug  = cpsf_get_site_forum_reply_slug();
		$reply_id    = 'cpsf_reply';

		parent::rewrite_rules( array(
			array(
				'regex' => $forum_regex . '/' . $tag_slug . '/([^/]+)/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $tag_id . '=$matches[2]&' . $page_id . '=$matches[3]',
			),
			array(
				'regex' => $forum_regex . '/' . $tag_slug . '/([^/]+)/' . $edit_slug . '/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $tag_id . '=$matches[2]&' . $edit_id . '=1',
			),
			array(
				'regex' => $forum_regex . '/' . $tag_slug . '/([^/]+)/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $tag_id . '=$matches[2]',
			),
			array(
				'regex' => $forum_regex . '/' . $reply_slug . '/([^/]+)/' . $edit_slug . '/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $reply_id . '=$matches[2]&' . $edit_id . '=1',
			),
			array(
				'regex' => $forum_regex . '/' . $reply_slug . '/([^/]+)/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $reply_id . '=$matches[2]',
			),
			array(
				'regex' => $forum_regex . '/' . $topic_slug . '/([^/]+)/' . $page_slug . '/?([0-9]{1,})/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $topic_id . '=$matches[2]&' . $page_id . '=$matches[3]',
			),
			array(
				'regex' => $forum_regex . '/' . $topic_slug . '/([^/]+)/' . $edit_slug . '/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $topic_id . '=$matches[2]&' . $edit_id . '=1',
			),
			array(
				'regex' => $forum_regex . '/' . $topic_slug . '/([^/]+)/?$',
				'query' => 'index.php?' . cp_site_get_rewrite_id() . '=$matches[1]&cp_site_forum=1&' . $topic_id . '=$matches[2]',
			),
		) );
	}

	/**
	 * Registers the Cluster's feedback messages.
	 *
	 * @since  1.0.0
	 *
	 * @return array The list of feedback messages.
	 */
	public function get_feedbacks() {
		if ( empty( $this->bbpress_loaded ) || ! cp_cluster_is_enabled( 'site' ) || ! is_multisite() ) {
			return array();
		}

		return array(
			'forum-01'  => array( 'cluster' => 'site', 'message' => __( 'Le forum a été sauvegardé avec succès.',                                   'clusterpress-sites-forum' ) ),
			'forum-02'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet de forum a été sauvegardé avec succès.',                          'clusterpress-sites-forum' ) ),
			'forum-03'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet a été ajouté à vos favoris avec succès.',                         'clusterpress-sites-forum' ) ),
			'forum-04'  => array( 'cluster' => 'site', 'message' => __( 'Votre abonnement au sujet a bien été enregistré.',                         'clusterpress-sites-forum' ) ),
			'forum-05'  => array( 'cluster' => 'site', 'message' => __( 'Votre abonnement au forum a bien été enregistré.',                         'clusterpress-sites-forum' ) ),
			'forum-06'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet a été supprimé de vos favoris avec succès.',                      'clusterpress-sites-forum' ) ),
			'forum-07'  => array( 'cluster' => 'site', 'message' => __( 'Votre abonnement au sujet a bien été supprimé.',                           'clusterpress-sites-forum' ) ),
			'forum-08'  => array( 'cluster' => 'site', 'message' => __( 'Votre abonnement au forum a bien été supprimé.',                           'clusterpress-sites-forum' ) ),
			'forum-09'  => array( 'cluster' => 'site', 'message' => __( 'Le forum a été supprimé.',                                                 'clusterpress-sites-forum' ) ),
			'forum-10'  => array( 'cluster' => 'site', 'message' => __( 'Un problème est survenu lors de la suppression du forum.',                 'clusterpress-sites-forum' ) ),
			'forum-11'  => array( 'cluster' => 'site', 'message' => __( 'Merci de confirmer la suppression du forum en activant la case à cocher.', 'clusterpress-sites-forum' ) ),
			'forum-12'  => array( 'cluster' => 'site', 'message' => __( 'L\'étiquette a été supprimée avec succès.',                                'clusterpress-sites-forum' ) ),
			'forum-13'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet a été mis à la corbeille.',                                       'clusterpress-sites-forum' ) ),
			'forum-14'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet a été marqué comme spam.',                                        'clusterpress-sites-forum' ) ),
			'forum-15'  => array( 'cluster' => 'site', 'message' => __( 'Le sujet est désormais fermé aux nouvelles réponses.',                     'clusterpress-sites-forum' ) ),
			'forum-16'  => array( 'cluster' => 'site', 'message' => __( 'Une erreur s\'est produite lors de la modification du sujet.',             'clusterpress-sites-forum' ) ),
		);
	}

	public function get_settings() {
		if ( ! cp_cluster_is_enabled( 'interactions' ) ) {
			return array();
		}

		return array(
			'sections' => array(
				'sites_forum_cluster_main_settings' => array(
					'title'    => __( 'Réglages principaux', 'clusterpress-sites-forum' ),
					'callback' => 'cp_core_main_settings_callback',
				),
			),
			'fields' => array(
				'sites_forum_cluster_main_settings' => array(
					'clusterpress_sites_forum_mentions' => array(
						'title'             => __( 'Mentions utilisateurs de ClusterPress', 'clusterpress-sites-forum' ),
						'callback'          => 'cpsf_mentions_settings_feature',
						'sanitize_callback' => 'intval',
					),
				),
			),
		);
	}
	/**
	 * Upgrade the plugin if needed.
	 *
	 * @since  1.0.0
	 */
	public function plugin_upgrade() {
		$db_version = get_network_option( 0, '_clusterpress_sites_forum_version', 0 );

		if ( version_compare( $db_version, $this->version, '<' ) ) {
			update_network_option( 0, '_clusterpress_sites_forum_version', $this->version );
		}
	}

	/**
	 * Install the Parent forum
	 *
	 * @since 1.0.0
	 */
	public function install() {
		if ( empty( $this->bbpress_loaded ) || ! cp_cluster_is_enabled( 'site' ) || ! is_multisite() ) {
			return;
		}

		$parent_forum_id = get_network_option( 0, '_clusterpress_sites_forum_parent_id', 0 );

		/**
		 * Create a parent forum that will be used to attach
		 * each site forum to it.
		 */
		if ( empty( $parent_forum_id ) ) {
			$parent_forum_id = bbp_insert_forum( array( 'post_title' => __( 'Forums des sites', 'clusterpress-sites-forum' ) ) );

			// Update the option
			update_network_option( 0, '_clusterpress_sites_forum_parent_id', $parent_forum_id );
		}
	}

	/**
	 * The forums Cluster needs to be activated after the site's one.
	 *
	 * If it's not the case, make sure to inform the user he needs
	 * to deactivate/reactivate it once the site's cluster is.
	 *
	 * @since 1.0.0
	 */
	public function please_deactivate() {
		if ( empty( $_GET['enable'] ) || 'site' !== $_GET['enable'] ) {
			return;
		}

		if ( ! cp_cluster_is_enabled( 'site' ) ) {
			set_site_transient( '_clusterpress_sites_forum_uninstall', 1, 30 );
		}

		remove_action( 'cp_set_clusters', array( $this, 'maybe_uninstall' ) );
    }

	/**
	 * Load Translations.
	 *
	 * @since 1.0.0
	 */
	public function load_languages() {
		$mofile = sprintf( $this->path_data['dir'] . 'languages/clusterpress-sites-forum-%s.mo', get_locale() );
		cp_load_textdomain( 'clusterpress-sites-forum', $mofile );
	}
}
