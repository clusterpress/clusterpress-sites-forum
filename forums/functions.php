<?php
/**
 * Functions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums
 * @subpackage functions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the plugin's version.
 *
 * @since  1.0.0
 *
 * @return string The plugin's version.
 */
function cpsf_get_plugin_version() {
	return clusterpress()->forums->version;
}

/**
 * Get the plugin's directory.
 *
 * @since  1.0.0
 *
 * @return string The plugin's directory.
 */
function cpsf_get_plugin_dir() {
	return clusterpress()->forums->path_data['dir'];
}

/**
 * Get the plugin's url.
 *
 * @since  1.0.0
 *
 * @return string The plugin's url.
 */
function cpsf_get_plugin_url() {
	return clusterpress()->forums->path_data['url'];
}

/**
 * Get the user's forum root slug.
 *
 * @since  1.0.0
 *
 * @return string The user's forum root slug.
 */
function cpsf_get_user_forums_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the user's forum root slug.
	 */
	$slug = apply_filters( 'cpsf_get_user_forums_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return bbp_get_root_slug();
}

/**
 * Get the user's topic slug.
 *
 * @since  1.0.0
 *
 * @return string The user's topic slug.
 */
function cpsf_get_user_topics_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the user's topic slug.
	 */
	$slug = apply_filters( 'cpsf_get_user_topics_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return bbp_get_topic_archive_slug();
}

/**
 * Get the user's replies slug.
 *
 * @since  1.0.0
 *
 * @return string The user's replies slug.
 */
function cpsf_get_user_replies_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the user's replies slug.
	 */
	$slug = apply_filters( 'cpsf_get_user_replies_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return bbp_get_reply_archive_slug();
}

/**
 * Get the user's favorites slug.
 *
 * @since  1.0.0
 *
 * @return string The user's favorites slug.
 */
function cpsf_get_user_favorites_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the user's favorites slug.
	 */
	$slug = apply_filters( 'cpsf_get_user_favorites_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return bbp_get_user_favorites_slug();
}

/**
 * Get the user's subscription manage slug.
 *
 * @since  1.0.0
 *
 * @return string The user's subscription manage slug.
 */
function cpsf_get_user_subscriptions_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the user's subscription manage slug.
	 */
	$slug = apply_filters( 'cpsf_get_user_subscriptions_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return bbp_get_root_slug();
}

/**
 * Get the site's topics archive (forum) slug.
 *
 * @since  1.0.0
 *
 * @return string The site's topics archive (forum) slug.
 */
function cpsf_get_site_topics_archive_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the site's topics archive (forum) slug.
	 */
	$slug = apply_filters( 'cpsf_get_site_topics_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( '_bbp_forum_slug', 'forum' );
}

/**
 * Get the site's manage forum slug.
 *
 * @since  1.0.0
 *
 * @return string The site's manage forum slug.
 */
function cpsf_get_site_manage_forum_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the site's manage forum slug.
	 */
	$slug = apply_filters( 'cpsf_get_site_topics_archive_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( '_bbp_forum_slug', 'forum' );
}

/**
 * Get the site's single topic slug.
 *
 * @since  1.0.0
 *
 * @return string The site's single topic slug.
 */
function cpsf_get_site_forum_topic_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the site's single topic slug.
	 */
	$slug = apply_filters( 'cpsf_get_site_forum_topic_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( '_bbp_topic_slug', 'topic' );
}

/**
 * Get the site's single reply slug.
 *
 * @since  1.0.0
 *
 * @return string The site's single reply slug.
 */
function cpsf_get_site_forum_reply_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the site's single reply slug.
	 */
	$slug = apply_filters( 'cpsf_get_site_forum_reply_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( '_bbp_reply_slug', 'reply' );
}

/**
 * Get the site's topic tag slug.
 *
 * @since  1.0.0
 *
 * @return string The site's topic tag slug.
 */
function cpsf_get_site_forum_tag_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the site's topic tag slug.
	 */
	$slug = apply_filters( 'cpsf_get_site_forum_tag_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( '_bbp_topic_tag_slug', 'topic-tag' );
}

/**
 * Checks if a user's forum section page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if viewing a user's forum section page. False otherwise.
 */
function cpsf_is_user_forum() {
	return cp_is_current_action( cpsf_get_user_forums_archive_slug() ) && cp_is_user();
}

/**
 * Checks if a user's manage forum section page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if viewing a user's manage forum section page. False otherwise.
 */
function cpsf_is_manage_user_forum() {
	return cp_is_current_sub_action( cpsf_get_user_subscriptions_archive_slug() ) && cp_is_user_manage();
}

/**
 * Checks if a site's forum section page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if viewing a site's forum section page. False otherwise.
 */
function cpsf_is_site_forum() {
	return cp_is_current_action( cpsf_get_site_topics_archive_slug() ) && cp_is_site();
}

/**
 * Checks if a topic search is performed on the displayed site..
 *
 * @since  1.0.0
 *
 * @return bool True if viewing the result a site's topic search. False otherwise.
 */
function cpsf_is_site_forum_search() {
	return cpsf_is_site_forum() && ! empty( $_GET['ts'] );
}

/**
 * Checks if a site's manage forum section page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if viewing a site's manage forum section page. False otherwise.
 */
function cpsf_is_manage_site_forum() {
	return cp_is_current_sub_action( cpsf_get_site_manage_forum_slug() ) && cp_is_site_manage();
}

function cpsf_is_clustepress_mentions_enabled( $default = true ) {
	return (bool) get_network_option( 0, 'clusterpress_sites_forum_mentions', $default );
}

/**
 * Sanitizes content outputs.
 *
 * @since  1.0.0
 *
 * @param  string $content The content to sanitize.
 * @return string          The sanitized content.
 */
function cpsf_kses( $content = '' ) {
	global $allowedtags;
	$allowed_tags = array_merge( $allowedtags, array(
		'ul' => true,
		'li' => true,
		'ol' => true,
		'img' => array(
			'src'    => true,
			'class'  => true,
			'alt'    => true,
			'height' => true,
			'width'  => true,
		),
		'span' => array(
			'class' => true,
 		),
 		'a' => array(
 			'href'  => true,
 			'title' => true,
 			'class' => true,
 		)
	) );

	return wp_kses( $content, $allowed_tags );
}

/**
 * ClusterPress Sites Forum mentions setting callback.
 *
 * @since  1.0.0
 *
 * @return HTML Output.
 */
function cpsf_mentions_settings_feature() {
	?>
	<input name="clusterpress_sites_forum_mentions" id="clusterpress-sites-forum-mentions" type="checkbox" value="1" <?php checked( cpsf_is_clustepress_mentions_enabled() ); ?> />
	<label for="clusterpress_sites_forum_mentions"><?php esc_html_e( 'Remplacer les mentions utilisateurs de bbPress par celles de ClusterPress.', 'clusterpress-sites-forum' ); ?></label>
	<p class="description">
		<?php esc_html_e( 'Lorsque vous optez pour ce réglage, les utilisateurs mentionnés seront &quot;pingés&quot; depuis la page listant les mentions de leur profil.', 'clusterpress-sites-forum' ); ?>
	</p>
	<?php
}
