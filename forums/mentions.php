<?php
/**
 * Mentions
 *
 * Overrides bbPress mentions with ClusterPress ones.
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums
 * @subpackage mentions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Disable bbPress mentions
 */
remove_filter( 'bbp_make_clickable', 'bbp_make_mentions_clickable', 8 );

/**
 * Add ClusterPress mentions support to topic and replies.
 *
 * @since 1.0.0
 */
function cpsf_add_post_types_support() {
	// Topics
	add_post_type_support( bbp_get_topic_post_type(), 'clusterpress_post_mentions' );

	// Replies
	add_post_type_support( bbp_get_reply_post_type(), 'clusterpress_post_mentions' );
}
add_action( 'cp_add_post_types_support', 'cpsf_add_post_types_support', 11 );

/**
 * Register the new user relationships
 *
 * @since  1.0.0
 */
function cpsf_register_forum_mentions() {
	cp_user_register_relationships( 'user', array(
		array(
			'name'             => bbp_get_topic_post_type() . '_mentions',
			'cluster'          => 'user',
			'singular'         => __( 'le sujet', 'clusterpress-sites-forum'  ),
			'plural'           => __( 'les sujets', 'clusterpress-sites-forum' ),
			'description'      => __( 'Mentions de sujet', 'clusterpress-sites-forum' ),
			'format_callback'  => array(
				'selfnew' => __( 'Vous avez été nouvellement mentionné dans un sujet de forum.', 'clusterpress-sites-forum' ),
				'self'    => __( 'Vous avez été mentionné dans un sujet de forum et en avez pris connaissance %s.', 'clusterpress-sites-forum' ),
				'read'    => __( '%1$s a été mentionné dans un sujet de forum et en a pris connaissance %2$s.', 'clusterpress-sites-forum' ),
				'unread'  => __( '%s a été mentionné dans un sujet de forum mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress-sites-forum' ),
			),
		),
		array(
			'name'             => bbp_get_reply_post_type() . '_mentions',
			'cluster'          => 'user',
			'singular'         => __( 'la réponse', 'clusterpress-sites-forum'  ),
			'plural'           => __( 'les réponses', 'clusterpress-sites-forum' ),
			'description'      => __( 'Mentions de réponse', 'clusterpress-sites-forum' ),
			'format_callback'  => array(
				'selfnew' => __( 'Vous avez été nouvellement mentionné dans une réponse de forum.', 'clusterpress-sites-forum' ),
				'self'    => __( 'Vous avez été mentionné dans une réponse de forum et en avez pris connaissance %s.', 'clusterpress-sites-forum' ),
				'read'    => __( '%1$s a été mentionné dans une réponse de forum et en a pris connaissance  %2$s.', 'clusterpress-sites-forum' ),
				'unread'  => __( '%s a été mentionné dans une réponse de forum mais n\'en a pas pris connaissance jusqu\'à présent.', 'clusterpress-sites-forum' ),
			),
		),
	) );
}
add_action( 'cp_register_relationships', 'cpsf_register_forum_mentions', 15 );

/**
 * Use one of the ClusterPress filter to format mentions in bbPress content.
 *
 * @since  1.0.0
 *
 * @param  string $forum_object_content The topic or the reply content.
 * @return string                       The topic or the reply content.
 */
function cpsf_apply_clusterpress_mentions_filter( $forum_object_content = '' ) {
	return apply_filters( 'cp_mentions_custom_post_type', $forum_object_content );
}
add_filter( 'bbp_make_clickable', 'cpsf_apply_clusterpress_mentions_filter', 8, 1 );

/**
 * Redirect replies to the topic, making sure the right page is displayed.
 *
 * @since  1.0.0
 *
 * @param  string $url     The original redirect url (permalink to the post type).
 * @param  object $mention The mention object.
 * @return string          The original redirect url or the paginated topic link in case of a reply.
 */
function cpsf_reply_mentions_redirect( $url = '', $mention = null ) {
	if ( empty( $mention->name ) ) {
		return $url;
	}

	// It's a topic, use the bbPress permalink function.
	if ( bbp_get_topic_post_type() . '_mentions' === $mention->name ) {
		return bbp_get_topic_permalink( $mention->primary_id );

	// It's not a bbPress object, stop here!
	} elseif ( bbp_get_reply_post_type() . '_mentions' !== $mention->name ) {
		return $url;
	}

	// We need to find the right page of the parent topic to redirect the user to.
	$topic_id   = bbp_get_reply_topic_id( $mention->primary_id );
	$topic_link = trailingslashit( bbp_get_topic_permalink( $topic_id ) );

	// Let's look for the reply position.
	bbp_has_replies( array(
		'post_parent' => $topic_id,
		'nopaging'    => true,
	) );

	$bbp = bbpress();
	if ( empty( $bbp->reply_query->posts ) ) {
		return $topic_link . '#post-' . $mention->primary_id;
	}

	$reply    = wp_list_filter( $bbp->reply_query->posts, array( 'ID' => $mention->primary_id ) );
	$position = key( $reply );
	$per_page = (int) bbp_get_replies_per_page();
	$page     = ceil( ( $position + 1 ) / $per_page );

	if ( 1 === $page ) {
		return $topic_link . '#post-' . $mention->primary_id;
	}

	if ( ! clusterpress()->permalink_structure ) {
		$url = add_query_arg( 'paged', $page, $topic_link );
	} else {
		$url = trailingslashit( $topic_link ) . cp_get_paged_slug() . '/' . $page . '/#post-' . $mention->primary_id;
	}

	return $url;
}
add_filter( 'cp_interactions_mention_redirect_customs', 'cpsf_reply_mentions_redirect', 10, 2 );
