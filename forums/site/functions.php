<?php
/**
 * Functions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums\site
 * @subpackage functions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the site's forum ID.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site Object
 * @return int           The site's forum ID.
 */
function cpsf_get_site_forum_id( $site = null ) {
	if ( empty( $site->blog_id ) ) {
		$site = cp_displayed_site();
	}

	if ( ! is_a( $site, 'WP_Site' ) ) {
		return 0;
	}

	if ( ! isset( $site->forum_id ) ) {
		$site->forum_id = (int) cp_sites_get_meta( $site->blog_id, '_cpsf_forum_id', true );
	}

	return $site->forum_id;
}

/**
 * Get the site's parent forum ID.
 *
 * @since  1.0.0
 *
 * @param  WP_Site $site The site Object
 * @return int           The site's parent forum ID.
 */
function cpsf_get_site_forum_parent_id( $site = null ) {
	if ( empty( $site->blog_id ) ) {
		$site = cp_displayed_site();
	}

	if ( ! is_a( $site, 'WP_Site' ) ) {
		return 0;
	}

	if ( ! isset( $site->forum_parent_id ) ) {
		$site->forum_parent_id = (int) get_network_option( 0, '_clusterpress_sites_forum_parent_id', 0 );
	}

	return $site->forum_parent_id;
}

/**
 * Checks if a site's single forum (topics archive) is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's single forum is displayed. False otherwise.
 */
function cpsf_is_single_forum() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_single_forum ) ) {
		$retval = (bool) $cpsf->is_single_forum;
	}

	return $retval;
}

/**
 * Checks if a site's manage forum section is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's manage forum section is displayed. False otherwise.
 */
function cpsf_is_forum_edit( $retval = false ) {
	if ( empty( $retval ) ) {
		$retval = (bool) cp_is_site_manage() && cp_is_current_sub_action( cpsf_get_site_manage_forum_slug() ) && cpsf_get_site_forum_id() ;
	}

	return $retval;
}

/**
 * Gets the site's forum object.
 *
 * @since  1.0.0
 *
 * @return null|WP_Post The Site's forum Object if it exists. Null otherwise.
 */
function cpsf_get_current_forum() {
	$forum = null;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->forum ) && is_a( $cpsf->forum, 'WP_Post' ) ) {
		$forum = $cpsf->forum;
	}

	return $forum;
}

/**
 * Checks if a site's single topic is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's single topic is displayed. False otherwise.
 */
function cpsf_is_single_topic() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_single_topic ) ) {
		$retval = (bool) $cpsf->is_single_topic;
	}

	return $retval;
}

/**
 * Checks if a site's topic edit page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's topic edit page is displayed. False otherwise.
 */
function cpsf_is_topic_edit() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_topic_edit ) ) {
		$retval = (bool) $cpsf->is_topic_edit;
	}

	return $retval;
}

/**
 * Gets a site's topic object.
 *
 * @since  1.0.0
 *
 * @return null|WP_Post A site's topic Object if it exists. Null otherwise.
 */
function cpsf_get_current_topic() {
	$topic = null;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->topic ) && is_a( $cpsf->topic, 'WP_Post' ) ) {
		$topic = $cpsf->topic;
	}

	return $topic;
}

/**
 * Gets a site's topic tag slug.
 *
 * @since  1.0.0
 *
 * @return false|string A site's topic tag slug if it exists. False otherwise.
 */
function cpsf_get_topic_tag() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_topic_tag ) ) {
		$retval = $cpsf->is_topic_tag;
	}

	return $retval;
}

/**
 * Gets a site's topic tag object.
 *
 * @since  1.0.0
 *
 * @return null|WP_Term A site's topic tag Object if it exists. Null otherwise.
 */
function cpsf_get_topic_tag_term() {
	$retval = null;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->topic_tag_term ) ) {
		$retval = $cpsf->topic_tag_term;
	}

	return $retval;
}

/**
 * Checks if a site's topic tag edit page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's topic tag edit page is displayed. False otherwise.
 */
function cpsf_is_topic_tag_edit() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_topic_tag_edit ) ) {
		$retval = (bool) $cpsf->is_topic_tag_edit;
	}

	return $retval;
}

/**
 * Checks if a site's single reply is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's single reply is displayed. False otherwise.
 */
function cpsf_is_single_reply() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_single_reply ) ) {
		$retval = (bool) $cpsf->is_single_reply;
	}

	return $retval;
}

/**
 * Checks if a site's reply edit page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's reply edit page is displayed. False otherwise.
 */
function cpsf_is_reply_edit() {
	$retval = false;

	$cpsf = clusterpress()->forums;

	if ( isset( $cpsf->is_reply_edit ) ) {
		$retval = (bool) $cpsf->is_reply_edit;
	}

	return $retval;
}

/**
 * Checks if the site's delete forum is requested.
 *
 * @since  1.0.0
 *
 * @return bool True if a site's delete forum is requested. False otherwise.
 */
function cpsf_is_forum_confirm_delete() {
	return cpsf_is_manage_site_forum() && ! empty( $_GET['confirm'] ) && 'delete' === $_GET['confirm'];
}
