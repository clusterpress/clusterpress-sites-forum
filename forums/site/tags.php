<?php
/**
 * Site Tags
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums\site
 * @subpackage tags
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Display the Title of the site's forum page.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_site_forum_title() {
	echo cpsf_get_site_forum_title();
}

	/**
	 * Get the Title of the site's forum page.
	 *
	 * @since  1.0.0
	 *
	 * @return string Site's forum page title.
	 */
	function cpsf_get_site_forum_title() {
		/**
		 * Filter here to edif the title of the site's forum page.
		 *
		 * @since  1.0.0
		 *
		 * @param string Site's forum page title.
		 */
		return apply_filters( 'cpsf_get_site_forum_title', esc_html__( 'Forum', 'clusterpress-sites-forum' ) );
	}

/**
 * Set a tag as the queried object.
 *
 * @since  1.0.0
 */
function cpsf_set_forum_queried_object() {
	global $wp_query;
	$cp = clusterpress();

	$tag = cpsf_get_topic_tag_term();

	if ( is_a( $tag, 'WP_Term' ) ) {
		$cp->forums->reset_queried_object_global = $wp_query->queried_object;
		$wp_query->queried_object = $tag;
	}
}

/**
 * Reset the queried object.
 *
 * @since  1.0.0
 */
function cpsf_reset_forum_queried_object() {
	global $wp_query;
	$cp = clusterpress();

	if ( ! empty( $cp->forums->reset_queried_object_global ) ) {
		$wp_query->queried_object = $cp->forums->reset_queried_object_global;
	}
}

/**
 * Set the type of forum object (topic/forum/reply).
 *
 * @since  1.0.0
 *
 * @param  string $object the name of the object.
 */
function cpsf_set_forum_object( $object = 'forum' ) {
	global $post;
	$cp = clusterpress();

	if ( is_a( $post, 'WP_Post') ) {
		$cp->forums->reset_post_global = $post;

		add_filter( "bbp_is_{$object}_edit", "cpsf_is_{$object}_edit" );
	} else {
		$cp->forums->reset_post_global = null;
	}

	if ( isset( $cp->forums->{$object}->ID ) && is_a( $cp->forums->{$object}, 'WP_Post') ) {
		$args = array(
			'p'              => $cp->forums->{$object}->ID,
			'posts_per_page' => 1,
			'nopaging'       => true,
		);

		if ( 'forum' === $object ) {
			$args['post_parent'] = cpsf_get_site_forum_parent_id();
		}

		$plural = $object . 's';
		if ( 'reply' === $object ) {
			$plural = 'replies';
		}

		if ( call_user_func_array( 'bbp_has_' .$plural, array( $args ) ) ) {
			call_user_func( 'bbp_' . $plural     );
			call_user_func( 'bbp_the_' . $object );
		}
	}

	if ( cpsf_get_topic_tag() ) {
		cpsf_set_forum_queried_object();
	}
}

/**
 * Reset the forum object type (topic/forum/reply).
 *
 * @since  1.0.0
 *
 * @param  string $object the name of the object.
 */
function cpsf_reset_forum_object( $object = 'forum' ) {
	global $post;
	$cp = clusterpress();

	if ( is_a( $cp->forums->reset_post_global, 'WP_Post') ) {
		$post = $cp->forums->reset_post_global;

		remove_filter( "bbp_is_{$object}_edit", "cpsf_is_{$object}_edit" );
	} else {
		$post = null;
	}

	if ( cpsf_get_topic_tag() ) {
		cpsf_reset_forum_queried_object();
	}
}

/**
 * Get the global forum object.
 *
 * @since  1.0.0
 *
 * @return WP_Post|False The forum object or False if not found.
 */
function cpsf_get_site_forum() {
	global $post;

	if ( ! isset( $post->post_type ) || $post->post_type !== bbp_get_forum_post_type() ) {
		return false;
	}

	return $post;
}

/**
 * Display the Parent forum ID.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_the_forum_parent_id() {
	echo esc_attr( cpsf_get_site_forum_parent_id() );
}

/**
 * Display the forum title.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_the_forum_title() {
	echo esc_attr( cpsf_get_forum_title() );
}

	/**
	 * Get the forum title.
	 *
	 * @since  1.0.0
	 *
	 * @return string The forum title.
	 */
	function cpsf_get_forum_title() {
		$title = cp_displayed_site()->name;
		$forum = cpsf_get_site_forum();

		if ( ! empty( $forum->post_title ) ) {
			$title = $forum->post_title;
		}

		/**
		 * Filter here to edit the forum name just before its display.
		 *
		 * @since 1.0.0
		 *
		 * @param  string $name The diplayed site name.
		 */
		return apply_filters( 'cpsf_get_forum_title', cp_displayed_site()->name );
	}

/**
 * Display the manage forum form url action.
 *
 * @since  1.0.0
 *
 * @return string URL of the form.
 */
function cpsf_the_redirect_url() {
	echo esc_url( cpsf_get_redirect_url() );
}

	/**
	 * Get the manage forum form url action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the form.
	 */
	function cpsf_get_redirect_url() {
		$redirect_url = cp_site_get_url( array(
			'slug'       => trailingslashit( cp_site_get_manage_slug() ) . cpsf_get_site_manage_forum_slug(),
			'rewrite_id' => cp_site_get_manage_rewrite_id(),
		), cp_displayed_site() );

		$redirect_url = add_query_arg( 'updated', 'site[forum-01]', $redirect_url );

		/**
		 * Filter here to edit the manage forum form url action.
		 *
		 * @since  1.0.0
		 *
		 * @param string URL of the form.
		 */
		return apply_filters( 'cpsf_get_redirect_url', $redirect_url );
	}

/**
 * Display the delete forum link.
 *
 * @since  1.0.0
 *
 * @param  int    $forum_id The forum ID.
 * @return string           URL of the delete forum link.
 */
function cpsf_forum_confirm_delete_link( $forum_id = 0 ) {
	 echo esc_url( cpsf_get_forum_confirm_delete_link( $forum_id ) );
}

	/**
	 * Get the delete forum link.
	 *
	 * @since  1.0.0
	 *
	 * @param  int    $forum_id The forum ID.
	 * @return string           URL of the delete forum link.
	 */
	function cpsf_get_forum_confirm_delete_link( $forum_id = 0 ) {
		if ( empty( $forum_id ) ) {
			$forum = cpsf_get_site_forum();

			if ( empty( $forum->ID ) ) {
				return false;
			}

			$forum_id = $forum->ID;
		}

		$confirm_delete_link = add_query_arg( 'confirm', 'delete', cp_site_get_url( array(
			'slug'       => trailingslashit( cp_site_get_manage_slug() ) . cpsf_get_site_manage_forum_slug(),
			'rewrite_id' => cp_site_get_manage_rewrite_id(),
		), cp_displayed_site() ) );

		/**
		 * Filter here to edit the delete forum link.
		 *
		 * @since  1.0.0
		 *
		 * @param string $confirm_delete_link URL of the delete forum link.
		 * @param int    $forum_id The forum ID.
		 */
		return apply_filters( 'cpsf_get_forum_confirm_delete_link', $confirm_delete_link, $forum_id );
	}

/**
 * Display the topic redirect url.
 *
 * @since  1.0.0
 *
 * @param  int    $topic_id The topic ID.
 * @return string           The topic redirect url.
 */
function cpsf_the_topic_redirect_url( $topic_id = 0 ) {
	echo esc_url( cpsf_get_topic_redirect_url( $topic_id ) );
}

	/**
	 * Get the topic redirect url.
	 *
	 * @since  1.0.0
	 *
	 * @param  int    $topic_id The topic ID.
	 * @return string           The topic redirect url.
	 */
	function cpsf_get_topic_redirect_url( $topic_id = 0 ) {
		if ( empty( $topic_id ) ) {
			$topic_id = bbp_get_topic_id();
		} elseif ( ! is_numeric( $topic_id ) && isset( $_POST['bbp_topic_id'] ) ) {
			$topic_id = (int) $_POST['bbp_topic_id'];
		}

		$redirect_url = cpsf_get_site_topic_permalink( '', $topic_id );

		if ( $redirect_url ) {
			$redirect_url = add_query_arg( 'updated', 'site[forum-02]', $redirect_url );
		} else {
			$redirect_url = '';
		}

		/**
		 * Filter here to edit the topic redirect url.
		 *
		 * @since  1.0.0
		 *
		 * @param string $redirect_url The topic redirect url.
		 */
		return apply_filters( 'cpsf_get_topic_redirect_url', $redirect_url );
	}
	add_filter( 'bbp_edit_topic_redirect_to', 'cpsf_get_topic_redirect_url', 10, 1 );

/**
 * Add a dashicon before the forum subscription link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_site_forum_subscription_link() {
	add_filter( 'bbp_is_single_forum', '__return_true' );

	bbp_forum_subscription_link( array(
		'before' => '<span class="dashicons dashicons-tickets-alt"></span>',
	) );

	remove_filter( 'bbp_is_single_forum', '__return_true' );
}

/**
 * Add a dashicon before the topic subscription link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_site_topic_subscription_link() {
	add_filter( 'bbp_is_single_topic', '__return_true' );

	bbp_topic_subscription_link( array(
		'before' => '<span class="dashicons dashicons-tickets-alt"></span>',
	) );

	remove_filter( 'bbp_is_single_topic', '__return_true' );
}

/**
 * Add a dashicon before the topic favorites link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cpsf_site_topic_favorites_link() {
	add_filter( 'bbp_is_single_topic', '__return_true' );

	bbp_topic_favorite_link( array(
		'before' => '<span class="dashicons dashicons-star-filled"></span>',
	) );

	remove_filter( 'bbp_is_single_topic', '__return_true' );
}

/**
 * Output the forum content to use it as an intro.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_the_site_single_forum_intro() {
	echo cpsf_get_site_single_forum_intro();
}

	/**
	 * Get the forum content to use it as an intro.
	 *
	 * @since  1.0.0
	 *
	 * @return string The forum intro.
	 */
	function cpsf_get_site_single_forum_intro() {
		$forum = cpsf_get_site_forum();

		/**
		 * Filter here to edit the forum intro.
		 *
		 * @since  1.0.0
		 *
		 * @return string $post_content The forum intro.
		 */
		return apply_filters( 'cpsf_site_forum_content', $forum->post_content );
	}

/**
 * Init a topics loop.
 *
 * @since  1.0.0
 *
 * @return bool True if the loop has topics to display. False otherwise.
 */
function cpsf_site_has_topics() {
	$args = array(
		'post_parent'    => cpsf_get_site_forum_id(),
		'show_stickies'  => true,
	);

	$topic_tag = cpsf_get_topic_tag();

	if ( $topic_tag ) {
		$args = array_merge( $args, array(
			'term'     => $topic_tag,
			'taxonomy' => bbp_get_topic_tag_tax_id(),
		) );

		add_filter( 'bbp_is_topic_tag', '__return_true' );
	}

	$bbp_q = bbp_has_topics( $args );

	if ( $topic_tag ) {
		remove_filter( 'bbp_is_topic_tag', '__return_true' );
	}

	return $bbp_q;
}

/**
 * Output the topic reply form.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_site_single_topic_new_reply() {
	add_filter( 'bbp_is_single_topic', '__return_true' );

	bbp_get_template_part( 'form', 'reply' );

	remove_filter( 'bbp_is_single_topic', '__return_true' );
}

/**
 * Output the forum topic form.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_site_single_forum_new_topic() {
	add_filter( 'bbp_is_single_forum', '__return_true' );

	bbp_get_template_part( 'form', 'topic' );

	remove_filter( 'bbp_is_single_forum', '__return_true' );
}

/**
 * Output a replies loop.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_site_single_topic_loop_replies() {
	add_filter( 'bbp_is_single_topic', '__return_true' );

	bbp_get_template_part( 'loop', 'replies' );

	remove_filter( 'bbp_is_single_topic', '__return_true' );
}

/**
 * Init a replies loop for the displayed topic.
 *
 * @since  1.0.0
 *
 * @return bool True if the topic has replies. False otherwise.
 */
function cpsf_topic_has_replies() {
	return bbp_has_replies( array(
		'post_parent' => cpsf_get_current_topic()->ID,
	) );
}

/**
 * Get the edit topic Tag link.
 *
 * @since  1.0.0
 *
 * @return string The edit topic Tag link.
 */
function cpsf_get_topic_tag_edit_link() {
	$edit_link = '';

	// If capable, include a link to edit the tag
	if ( current_user_can( 'manage_topic_tags' ) ) {
		$edit_link .= '<li><a href="' . esc_url( bbp_get_topic_tag_edit_link() ) . '" class="bbp-edit-topic-tag-link"><span class="dashicons dashicons-edit"></span> ' . esc_html__( 'Modifier l\'étiquette', 'clusterpress-sites-forum' ) . '</a></li>';
	}

	return $edit_link;
}

/**
 * Display the topic Tag description.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cpsf_the_topic_tag_description() {
	echo cpsf_get_topic_tag_description();
}

	/**
	 * Get the topic Tag description.
	 *
	 * @since  1.0.0
	 *
	 * @return string The topic Tag description.
	 */
	function cpsf_get_topic_tag_description() {
		$edit_link = cpsf_get_topic_tag_edit_link();

		$topic_tag_description = bbp_get_topic_tag_description( array(
			'before' => '<ul><li>',
			'after'  => sprintf( '</li>%s</ul>', $edit_link ),
		) );

		if ( ! $topic_tag_description ) {
			$topic_tag_description = sprintf( '<ul>%s</ul>', $edit_link );
		}

		/**
		 * Filter here to edit the topic Tag description.
		 *
		 * @since  1.0.0
		 *
		 * @param string The topic Tag description.
		 */
		return apply_filters( 'cpsf_get_topic_tag_description', $topic_tag_description );
	}

/**
 * Display the Topic Edit form action.
 *
 * @since  1.0.0
 *
 * @return string URL of the topic edit form action.
 */
function cpsf_the_topic_edit_form_action() {
	echo esc_url( cpsf_get_topic_edit_form_action() );
}

	/**
	 * Get the Topic Edit form action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the topic edit form action.
	 */
	function cpsf_get_topic_edit_form_action() {
		$topic_id = bbp_get_topic_id();

		if ( empty( $topic_id ) ) {
			return '';
		}

		return bbp_get_topic_edit_url();
	}

/**
 * Display the Reply Edit form action.
 *
 * @since  1.0.0
 *
 * @return string URL of the reply edit form action.
 */
function cpsf_the_reply_edit_form_action() {
	echo esc_url( cpsf_get_reply_edit_form_action() );
}

	/**
	 * Get the Reply Edit form action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the reply edit form action.
	 */
	function cpsf_get_reply_edit_form_action() {
		$reply_id = bbp_get_reply_id();

		if ( empty( $reply_id ) ) {
			return '';
		}

		return bbp_get_reply_edit_url();
	}

/**
 * Display the merge topic form action.
 *
 * @since  1.0.0
 *
 * @return string URL of the merge topic form action.
 */
function cpsf_the_topic_merge_form_action() {
	echo esc_url( cpsf_get_topic_merge_form_action() );
}

	/**
	 * Get the merge topic form action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the merge topic form action.
	 */
	function cpsf_get_topic_merge_form_action() {
		$topic_id = bbp_get_topic_id();

		if ( empty( $topic_id ) ) {
			return '';
		}

		return add_query_arg( 'action', 'merge', bbp_get_topic_edit_url() );
	}

/**
 * Display the split topic form action.
 *
 * @since  1.0.0
 *
 * @return string URL of the split topic form action.
 */
function cpsf_the_topic_split_form_action() {
	echo esc_url( cpsf_get_topic_split_form_action() );
}

	/**
	 * Get the split topic form action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the split topic form action.
	 */
	function cpsf_get_topic_split_form_action() {
		$topic_id = bbp_get_topic_id();

		if ( empty( $topic_id ) ) {
			return '';
		}

		$query_args = array( 'action' => 'split' );
		if ( ! empty( $_GET['reply_id'] ) ) {
			$query_args['reply_id'] = (int) $_GET['reply_id'];
		}

		return add_query_arg( $query_args, bbp_get_topic_edit_url() );
	}

/**
 * Display the move reply form action.
 *
 * @since  1.0.0
 *
 * @return string URL of the move reply form action.
 */
function cpsf_the_reply_move_form_action() {
	echo esc_url( cpsf_get_reply_move_form_action() );
}

	/**
	 * Get the move reply form action.
	 *
	 * @since  1.0.0
	 *
	 * @return string URL of the move reply form action.
	 */
	function cpsf_get_reply_move_form_action() {
		$reply_id = bbp_get_reply_id();

		if ( empty( $reply_id ) ) {
			return '';
		}

		return add_query_arg( array( 'action' => 'move', 'reply_id' => $reply_id ), bbp_get_reply_edit_url() );
	}

/**
 * Check if there are topics to merge or split.
 *
 * @since  1.0.0
 *
 * @return bool True if there are topics to merge or split. False otherwise.
 */
function cpsf_has_topics_to_merge_or_split() {
	$bbp = bbpress();

	if ( empty( $bbp->current_topic_id ) ) {
		$bbp->current_topic_id = bbp_get_topic_id();
	}

	return bbp_has_topics( array(
		'show_stickies' => false,
		'post_parent'   => bbp_get_topic_forum_id( $bbp->current_topic_id ),
		'post__not_in'  => array( $bbp->current_topic_id )
	) );
}

/**
 * Check if there are topics to move the reply in.
 *
 * @since  1.0.0
 *
 * @return bool True if there are topics to move the reply in. False otherwise.
 */
function cpsf_has_topics_to_move_in() {
	$bbp = bbpress();

	if ( empty( $bbp->current_reply_id ) ) {
		$bbp->current_reply_id = bbp_get_reply_id();
	}

	return bbp_has_topics( array(
		'show_stickies' => false,
		'post_parent'   => bbp_get_reply_forum_id( $bbp->current_reply_id ),
		'post__not_in'  => array( bbp_get_reply_topic_id( $bbp->current_reply_id ) )
	) );
}
