<?php
/**
 * Actions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums\site
 * @subpackage actions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Registers a new section inside Sites discovery pages.
 *
 * @since  1.0.0
 */
function cpsf_register_site_section() {
	// Registers The Site's discovery Forum section
	cp_register_cluster_section( 'site', array(
		'id'               => 'site-forum',
		'name'             => bbp_get_forum_archive_title(),
		'cluster_id'       => 'site',
		'slug'             => cpsf_get_site_topics_archive_slug(),
		'rewrite_id'       => 'cp_site_forum',
		'template'         => 'site/single/forum',
		'template_dir'     => trailingslashit( cpsf_get_plugin_dir() ) . 'templates',
		'toolbar_items'    => array(
			'position'     => 50,
			'dashicon'     => 'dashicons-megaphone',
			'children'     => array(
				array(
					'id'           => 'manage-site-forum',
					'slug'         => cpsf_get_site_manage_forum_slug(),
					'parent'       => cp_site_get_manage_rewrite_id(),
					'parent_group' => 'single-bar',
					'title'        => __( 'Forum', 'clusterpress-sites-forum' ),
					'href'         => '#',
					'position'     => 40,
					'capability'   => 'cp_edit_single_site',
				),
			),
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cpsf_register_site_section', 14 );

/**
 * Adds metas to forum and sites to inform about their association.
 *
 * @since  1.0.0
 *
 * @param  int $forum_id The Forum ID
 */
function cpsf_site_add_forum_metas( $forum_id = 0 ) {
	if ( empty( $forum_id ) || ! cp_is_site_manage() || ! cp_is_current_sub_action( cpsf_get_site_manage_forum_slug() ) ) {
		return;
	}

	$site_id = cp_get_displayed_site_id();

	if ( empty( $site_id ) ) {
		return;
	}

	cp_sites_update_meta( $site_id, '_cpsf_forum_id', $forum_id );
	update_post_meta( $forum_id, '_cpsf_site_id', $site_id );
}
add_action( 'bbp_new_forum_post_extras', 'cpsf_site_add_forum_metas', 10, 1 );

/**
 * Redirect the User once he favorited or subscribed to a bbPress object of the displayed site.
 *
 * @since  1.0.0
 *
 * @param  bool    $success   True if the favorite or subscribe action was successful. False otherwise.
 * @param  int     $user_id   The user ID.
 * @param  int     $object_id The forum or topic ID.
 * @param  string  $action    The user action (favorite/unfavorite/subscribe/unsubscribe).
 */
function cpsf_site_redirect_after_subfav( $success = false, $user_id = 0, $object_id = 0, $action = '' ) {
	if ( empty( $success ) || empty( $action ) || ! cp_is_site() ) {
		return;
	}

	$referer  = remove_query_arg( array( 'action', 'forum_id', 'topic_id', '_wpnonce' ), wp_get_referer() );
	$is_topic = cpsf_is_single_topic();

	$query_args = array();

	switch ( $action ) {
		case 'bbp_unsubscribe'     :
			$query_args = array( 'updated' => 'site[forum-08]' );

			if ( $is_topic ) {
				$query_args = array( 'updated' => 'site[forum-07]' );
			}
			break;

		case 'bbp_subscribe'       :
			$query_args = array( 'updated' => 'site[forum-05]' );

			if ( $is_topic ) {
				$query_args = array( 'updated' => 'site[forum-04]' );
			}
			break;

		case 'bbp_favorite_remove' :
			$query_args = array( 'updated' => 'site[forum-06]' );
			break;

		case 'bbp_favorite_add'    :
			$query_args = array( 'updated' => 'site[forum-03]' );
			break;
	}

	wp_redirect( add_query_arg( $query_args, $referer ) );
	exit();
}
add_action( 'bbp_subscriptions_handler', 'cpsf_site_redirect_after_subfav', 10, 4 );
add_action( 'bbp_favorites_handler',     'cpsf_site_redirect_after_subfav', 10, 4 );

/**
 * Deletes the forum of a site.
 *
 * @since  1.0.0
 */
function cpsf_site_delete_forum() {
	if ( ! cpsf_is_manage_site_forum() || empty( $_POST['cp_sites_forum'] ) ) {
		return;
	}

	$redirect = wp_get_referer();

	if ( empty( $_POST['cp_sites_forum']['confirm'] ) ) {
		cp_feedback_redirect( 'forum-11', 'error', $redirect );
	}

	check_admin_referer( 'cp-sites-forum-delete' );

	$redirect = remove_query_arg( array( 'confirm' ), $redirect );

	$site = cp_displayed_site();
	if ( ! current_user_can( 'cp_edit_single_site', array( 'site' => $site ) )
		|| empty( $site->blog_id ) || empty( $site->forum_id )
		|| empty( $site->forum_parent_id ) ) {
		cp_feedback_redirect( 'not-allowed-01', 'error', $redirect );
	}

	do_action( 'cpsf_before_delete_forum', $site );

	$forum_id = wp_update_post( array(
		'ID'          => $site->forum_id,
		'post_parent' => 0,
	) );

	if ( is_wp_error( $forum_id ) ) {
		cp_feedback_redirect( 'forum-10', 'error', $redirect );
	}

	if ( ! wp_trash_post( $forum_id ) ) {
		cp_feedback_redirect( 'forum-10', 'error', $redirect );
	}

	do_action( 'cpsf_deleted_forum', $forum_id, $site );

	// Finally redirect and add a success message.
	cp_feedback_redirect( 'forum-09', 'updated', $redirect );
}
add_action( 'cp_page_actions', 'cpsf_site_delete_forum' );

/**
 * Removes the association between a site and a forum when forum is trashed.
 *
 * @since  1.0.0
 *
 * @param  int $forum_id The forum ID we need to remove the site association from.
 */
function cpsf_site_delete_association( $forum_id = 0 ) {
	$post = get_post( $forum_id );

	if ( empty( $post->post_type ) || bbp_get_forum_post_type() !== $post->post_type ) {
		return;
	}

	// Is the forum associated to a Cluster Site ?
	$site_id = (int) get_post_meta( $post->ID, '_cpsf_site_id', true );

	if ( ! $site_id ) {
		return;
	}

	// Delete the forum's association with the site
	delete_post_meta( $forum_id, '_cpsf_site_id', $site_id );

	// Delete the site's association with the forum
	cp_sites_delete_meta( $site_id, '_cpsf_forum_id', $forum_id );
}
add_action( 'wp_trash_post', 'cpsf_site_delete_association', 10, 1 );

/**
 * Redirect the user to the site's forum home once a tag has been deleted.
 *
 * @since  1.0.0
 */
function cpsf_site_redirect_after_tag_delete() {
	$site = cp_displayed_site();

	if ( empty( $site->blog_id ) ) {
		return;
	}

	$redirect = add_query_arg( 'updated', 'site[forum-12]', cp_site_get_url( array(
		'slug'       => cpsf_get_site_topics_archive_slug(),
		'rewrite_id' => 'cp_site_forum',
	), $site ) );

	wp_redirect( $redirect );
	exit();
}
add_action( 'bbp_delete_topic_tag', 'cpsf_site_redirect_after_tag_delete' );
