<?php
/**
 * Filters
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums\site
 * @subpackage filters
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Redirects the user to the Site's discovery page in case bbPress objects are linked to a site.
 *
 * @since  1.0.0
 *
 * @param  string $url The redirect url.
 * @return string      The edited redirect url.
 */
function cpsf_site_redirect_canonical( $url = '' ) {
	// Viewing a single forum
	if ( bbp_is_single_forum() ) {
		$url = cpsf_get_site_forum_permalink( '', get_the_ID() );

	// Viewing a single topic
	} elseif ( bbp_is_single_topic() ) {
		$url = cpsf_get_site_topic_permalink( '', get_the_ID() );
	}

	return $url;
}
add_filter( 'cpsf_redirect_canonical', 'cpsf_site_redirect_canonical', 10, 1 );

/**
 * Filters the displayed site's navigation to remove the forum nav if no forum are linked to the site
 *
 * @since  1.0.0
 *
 * @param  array   $nodes  The list of the site's nav items.
 * @param  WP_Site $site   The Site object.
 * @return array           The edited list of the site's nav items.
 */
function cpsf_site_edit_toolbar( $nodes, $site = null ) {
	if ( empty( $site->blog_id ) ) {
		return;
	}

	// Get ClusterPress instance
	$cp = clusterpress();

	$forum_id = (int) cp_sites_get_meta( $site->blog_id, '_cpsf_forum_id', true );

	// Remove the Forum node if no forum is attached to the site
	if ( ! $forum_id ) {
		// Remove the node
		$cp->site->toolbar->remove_node( 'cp_site_forum' );

		// Make sure to get a 404 if the url is accessed directly
		unset( $nodes['cp_site_forum'] );

	// Else add the Forum ID to the Site's global
	} else {
		clusterpress()->cluster->displayed_object->forum_id = $forum_id;
	}

	clusterpress()->cluster->displayed_object->forum_parent_id = get_network_option( 0, '_clusterpress_sites_forum_parent_id', 0 );

	return $nodes;
}
add_filter( 'cp_site_cluster_get_toolbar_urls', 'cpsf_site_edit_toolbar', 10, 2 );

/**
 * Get the permalink to a Site's forum
 *
 * @since  1.0.0
 *
 * @param  string $permalink The permalink of the forum.
 * @param  int    $forum_id  The forum ID.
 * @return string            The permalink to the site's forum (if needed).
 */
function cpsf_get_site_forum_permalink( $permalink = '', $forum_id = 0 ) {
	if ( empty( $forum_id ) ) {
		return $permalink;
	}

	// Then get the site_id
	$site_id = (int) get_post_meta( $forum_id, '_cpsf_site_id', true );

	if ( ! $site_id ) {
		return $permalink;
	}

	// Are we on a site ?
	$site = cp_displayed_site();

	// We are on site, but if the forum is not attached to it, reset the site
	if ( empty( $site->forum_id ) || (int) $site->forum_id !== (int) $forum_id ) {
		$site = null;
	}

	$forum_url = cp_site_get_url( array(
		'rewrite_id'    => 'cp_site_forum',
		'slug'          => cpsf_get_site_topics_archive_slug(),
		'site_id'       => $site_id,
	), $site );

	if ( $forum_url ) {
		$old_permalink = $permalink;
		$permalink     = $forum_url;
	}

	/**
	 * Filter here to edit the site forum permalink.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $permalink     The permalink of the forum.
	 * @param  string $old_permalink The original bbPress permalink of the forum.
	 * @param  int    $forum_id      The forum ID.
	 * @param  int    $site_id       The Site ID.
	 */
	return apply_filters( 'cpsf_get_site_forum_permalink', $permalink, $old_permalink, $forum_id, $site_id );
}
add_filter( 'bbp_get_forum_permalink', 'cpsf_get_site_forum_permalink', 10, 2 );

/**
 * Get the permalink of a topic belonging to a site's forum.
 *
 * @since  1.0.0
 *
 * @param  string $permalink The permalink of the topic.
 * @param  int    $topic_id  The topic ID.
 * @return string            The permalink to the site's topic (if needed).
 */
function cpsf_get_site_topic_permalink( $permalink = '', $topic_id = 0 ) {
	if ( empty( $topic_id ) ) {
		return $permalink;
	}

	// First get the forum id
	$forum_id = bbp_get_topic_forum_id( $topic_id );

	if ( ! $forum_id ) {
		return $permalink;
	}

	$forum_permalink = cpsf_get_site_forum_permalink( '', $forum_id );

	if ( ! $forum_permalink ) {
		return $permalink;
	}

	$topic_slug    = get_post_field( 'post_name', $topic_id );
	$topic_status  = get_post_status( $topic_id );
	$old_permalink = $permalink;

	if ( ! clusterpress()->permalink_structure ) {
		$permalink = esc_url_raw( add_query_arg( 'cpsf_topic', $topic_slug, $forum_permalink ) );
	} else {
		$permalink = trailingslashit( $forum_permalink ) . cpsf_get_site_forum_topic_slug() . '/' . trailingslashit( $topic_slug );
	}

	// Handle specific cases when a topic is trashed or spammed.
	if ( 'publish' !== $topic_status && ! empty( $_GET['action'] ) ) {
		if ( 'bbp_toggle_topic_trash' === $_GET['action'] ) {
			$reditect_args = array( 'updated' => 'site[forum-13]' );
		} elseif ( 'bbp_toggle_topic_spam' === $_GET['action'] ) {
			$reditect_args = array( 'updated' => 'site[forum-14]' );
		} elseif ( 'bbp_toggle_topic_close' === $_GET['action'] ) {
			$reditect_args = array( 'updated' => 'site[forum-15]' );
		} else {
			$reditect_args = array( 'error' => 'site[forum-16]' );
		}

		$permalink = esc_url_raw( add_query_arg( $reditect_args, $permalink ) );
	}

	/**
	 * Filter here to edit the site topic permalink.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $permalink     The permalink of the topic.
	 * @param  string $old_permalink The original bbPress permalink of the topic.
	 * @param  int    $topic_id      The Topic ID.
	 * @param  int    $forum_id      The forum ID.
	 */
	return apply_filters( 'cpsf_get_site_topic_permalink', $permalink, $old_permalink, $topic_id, $forum_id );
}
add_filter( 'bbp_get_topic_permalink', 'cpsf_get_site_topic_permalink', 10, 2 );

/**
 * Get the edit url of a site's topic.
 *
 * @since  1.0.0
 *
 * @param  string  $permalink The topic edit url.
 * @param  int     $topic_id  The topic ID.
 * @return string             The edit url of the site's topic (if needed).
 */
function cpsf_get_site_topic_edit_url( $permalink = '', $topic_id = 0 ) {
	if ( empty( $topic_id ) || clusterpress()->permalink_structure ) {
		return $permalink;
	}

	$topic_link = cpsf_get_site_topic_permalink( '', $topic_id );

	if ( ! $topic_link ) {
		return $permalink;
	}

	$old_permalink = $permalink;
	$permalink     = add_query_arg( 'cpsf_edit', 1, $topic_link );

	/**
	 * Filter here to edit the site topic edit url.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $permalink     The edit url of the topic.
	 * @param  string $old_permalink The original bbPress edit url of the topic.
	 * @param  int    $topic_id      The Topic ID.
	 */
	return apply_filters( 'cpsf_get_site_topic_edit_url', $permalink, $old_permalink, $topic_id );
}
add_filter( 'bbp_get_topic_edit_url', 'cpsf_get_site_topic_edit_url', 10, 2 );

/**
 * Get the permalink of a reply belonging to a site's forum.
 *
 * @since  1.0.0
 *
 * @param  string $permalink The permalink of the reply.
 * @param  int    $reply_id  The reply ID.
 * @return string            The permalink to the site's reply (if needed).
 */
function cpsf_get_site_reply_permalink( $permalink = '', $reply_id = 0 ) {
	if ( empty( $reply_id ) ) {
		return $permalink;
	}

	$forum_id = bbp_get_reply_forum_id( $reply_id );

	if ( ! $forum_id ) {
		return $permalink;
	}

	$forum_permalink = cpsf_get_site_forum_permalink( '', $forum_id );

	if ( ! $forum_permalink ) {
		return $permalink;
	}

	$old_permalink = $permalink;

	if ( ! clusterpress()->permalink_structure ) {
		$permalink = esc_url_raw( add_query_arg( 'cpsf_reply', $reply_id, $forum_permalink ) );
	} else {
		$permalink = trailingslashit( $forum_permalink ) . cpsf_get_site_forum_reply_slug() . '/' . trailingslashit( $reply_id );
	}

	/**
	 * Filter here to edit the site topic reply url.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $permalink     The permalink of the reply.
	 * @param  string $old_permalink The original bbPress permalink of the reply.
	 * @param  int    $reply_id      The Reply ID.
	 * @param  int    $forum_id      The forum ID.
	 */
	return apply_filters( 'cpsf_get_site_reply_permalink', $permalink, $old_permalink, $reply_id, $forum_id );
}
add_filter( 'bbp_get_reply_permalink', 'cpsf_get_site_reply_permalink', 10, 2 );

/**
 * Get the edit url of a site's reply.
 *
 * @since  1.0.0
 *
 * @param  string  $permalink The reply edit url.
 * @param  int     $reply_id  The reply ID.
 * @return string             The edit url of the site's reply (if needed).
 */
function cpsf_get_site_reply_edit_url( $permalink = '', $reply_id = 0 ) {
	if ( empty( $reply_id ) || clusterpress()->permalink_structure ) {
		return $permalink;
	}

	$reply_link = cpsf_get_site_reply_permalink( '', $reply_id );

	if ( ! $reply_link ) {
		return $permalink;
	}

	$old_permalink = $permalink;
	$permalink     = add_query_arg( 'cpsf_edit', 1, $reply_link );

	/**
	 * Filter here to edit the site topic reply edit url.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $permalink     The edit url of the reply.
	 * @param  string $old_permalink The original bbPress edit url of the reply.
	 * @param  int    $reply_id      The Reply ID.
	 */
	return apply_filters( 'cpsf_get_site_reply_edit_url', $permalink, $old_permalink, $reply_id );
}
add_filter( 'bbp_get_reply_edit_url', 'cpsf_get_site_reply_edit_url', 10, 2 );

/**
 * Neutralize the Super Stickies on sites.
 *
 * @since  1.0.0
 *
 * @param  string $retval The Sticky links.
 * @return string         The Sticky links.
 */
function cpsf_get_topic_stick_link( $retval ) {
	if ( ! cp_is_site() ) {
		return $retval;
	}

	preg_match_all( '/<a\s[^>]*>(.*)<\/a>/siU', $retval, $as );

	if ( ! empty( $as[0][0] ) ) {
		$retval = $as[0][0];
	}

	return $retval;
}
add_filter( 'bbp_get_topic_stick_link', 'cpsf_get_topic_stick_link', 10, 1 );

/**
 * Get the permalink of a topic tag belonging to a site's forum.
 *
 * @since  1.0.0
 *
 * @param  string   $tag_link The permalink of the topic tag.
 * @param  WP_Term  $tag      The term object.
 * @param  string   $taxonomy The taxonomy ID.
 * @return string             The permalink to the site's topic tag (if needed).
 */
function cpsf_get_topic_tag_link( $tag_link = '', $tag = null, $taxonomy = '' ) {
	if ( empty( $tag->slug ) || empty( $taxonomy ) || bbp_get_topic_tag_tax_id() !== $taxonomy ) {
		return $tag_link;
	}

	$site = cp_displayed_site();

	if ( ! $site ) {
		return $tag_link;
	}

	$old_permalink = $tag_link;

	$forum_permalink = cp_site_get_url( array(
		'rewrite_id'    => 'cp_site_forum',
		'slug'          => cpsf_get_site_topics_archive_slug(),
	), $site );

	if ( ! clusterpress()->permalink_structure ) {
		$permalink = esc_url_raw( add_query_arg( 'cpsf_topic_tag', $tag->slug, $forum_permalink ) );
	} else {
		$permalink = trailingslashit( $forum_permalink ) . cpsf_get_site_forum_tag_slug() . '/' . trailingslashit( $tag->slug );
	}

	return apply_filters( 'cpsf_get_topic_tag_link', $permalink, $old_permalink );
}
add_filter( 'term_link', 'cpsf_get_topic_tag_link', 10, 3 );

/**
 * Makes sure the Topic Tags are pre populated the reply form.
 *
 * @since  1.0.0
 *
 * @param  string $topic_tags The comma separated value of Tags.
 * @return string             The comma separated value of Tags.
 */
function cpsf_get_form_topic_tags( $topic_tags = '' ) {
	if ( ! empty( $topic_tags ) || ! cpsf_is_single_topic() ) {
		return $topic_tags;
	}

	$topic_id = bbp_get_topic_id();

	if ( empty( $topic_id ) ) {
		return $topic_tags;
	}

	// Topic is spammed so display pre-spam terms
	if ( bbp_is_topic_spam( $topic_id ) ) {

		// Get pre-spam terms
		$spam_terms = get_post_meta( $topic_id, '_bbp_spam_topic_tags', true );
		$topic_tags = ( ! empty( $spam_terms ) ) ? join( ', ', $spam_terms ) : '';

	// Topic is not spam so get real terms
	} else {
		$topic_tags = bbp_get_topic_tag_names( $topic_id );
	}

	/**
	 * Filter here to edit the topic tags to output in the form.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $topic_tags    The comma separated value of Tags.
	 * @param  int    $topic_id      The Topic ID.
	 */
	return apply_filters( 'cpsf_get_form_topic_tags', $topic_tags, $topic_id );
}
add_filter( 'bbp_get_form_topic_tags', 'cpsf_get_form_topic_tags', 10, 1 );

/**
 * Filters for the output contents.
 */
add_filter( 'cpsf_site_forum_content', 'cpsf_kses'                         );
add_filter( 'cpsf_site_forum_content', 'wp_unslash'                        );
add_filter( 'cpsf_site_forum_content', 'wptexturize'                       );
add_filter( 'cpsf_site_forum_content', 'convert_smilies',               20 );
add_filter( 'cpsf_site_forum_content', 'wpautop'                           );
add_filter( 'cpsf_site_forum_content', 'do_shortcode',                  11 );
add_filter( 'cpsf_site_forum_content', 'shortcode_unautop'                 );
add_filter( 'cpsf_site_forum_content', 'prepend_attachment'                );
add_filter( 'cpsf_site_forum_content', 'wp_make_content_images_responsive' );

add_filter( 'cpsf_get_topic_tag_description', 'cpsf_kses'           );
add_filter( 'cpsf_get_topic_tag_description', 'wp_unslash'          );
add_filter( 'cpsf_get_topic_tag_description', 'wptexturize'         );
add_filter( 'cpsf_get_topic_tag_description', 'convert_smilies', 20 );

/**
 * Map forum caps for the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $caps    List of Caps for the current capability check.
 * @param  string $cap     The current capability check.
 * @param  int    $user_id The current user ID.
 * @param  array  $args    Additional arguments for the capability check.
 * @return array           List of Caps for the current capability check.
 */
function cpsf_map_forum_caps( $caps, $cap, $user_id, $args ) {
	if ( ! cp_displayed_site() ) {
		return $caps;
	}

	$is_site_admin = is_super_admin();
	if ( ! $is_site_admin && false !== array_search( $user_id, cp_displayed_site()->admins ) ) {
		$is_site_admin = true;
	}

	switch ( $cap ) {
		case 'publish_forums' :
			if ( $is_site_admin ) {
				$caps = array( 'exist' );
			}
			break;

		case 'edit_forum'     :
		case 'delete_forum'   :
			$_forum = get_post( $args[0] );

			if ( (int) $_forum->ID === (int) cp_displayed_site()->forum_id && $is_site_admin ) {
				$caps = array( 'exist' );
			}
			break;
	}

	/**
	 * Filter here to edit the forum caps map.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $caps    List of Caps for the current capability check.
	 * @param  string $cap     The current capability check.
	 * @param  int    $user_id The current user ID.
	 * @param  array  $args    Additional arguments for the capability check.
	 */
	return apply_filters( 'cpsf_map_forum_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'bbp_map_forum_meta_caps', 'cpsf_map_forum_caps', 10, 4 );

/**
 * Map topic caps for the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $caps    List of Caps for the current capability check.
 * @param  string $cap     The current capability check.
 * @param  int    $user_id The current user ID.
 * @param  array  $args    Additional arguments for the capability check.
 * @return array           List of Caps for the current capability check.
 */
function cpsf_map_topic_caps( $caps, $cap, $user_id, $args ) {
	if ( ! cp_displayed_site() ) {
		return $caps;
	}

	$is_site_admin = is_super_admin();
	if ( ! $is_site_admin && false !== array_search( $user_id, cp_displayed_site()->admins ) ) {
		$is_site_admin = true;
	}

	switch ( $cap ) {
		case 'publish_topics' :
			if ( ! $is_site_admin && ! empty( $user_id ) ) {
				$user_sites = cp_sites_get_user_sites( $user_id, 'any' );

				// Only site members of followers can publish topics
				if ( false === array_search( cp_get_displayed_site_id(), $user_sites ) ) {
					$caps = array( 'do_not_allow' );
				} else {
					$caps = array( 'exist' );
				}
			}
			break;

		case 'edit_topic'           :
		case 'edit_topics'          :
		case 'edit_others_topics'   :
		case 'delete_topics'        :
		case 'delete_others_topics' :
			if ( $is_site_admin ) {
				$caps = array( 'exist' );
			}
			break;

		case 'moderate'     :
		case 'delete_topic' :
			if ( ! empty( $args[0] ) ) {
				$_topic = get_post( $args[0] );

				if ( (int) bbp_get_topic_forum_id( $_topic->ID ) === (int) cp_displayed_site()->forum_id && $is_site_admin ) {
					$caps = array( 'exist' );
				}
			}
			break;
	}

	/**
	 * Filter here to edit the topic caps map.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $caps    List of Caps for the current capability check.
	 * @param  string $cap     The current capability check.
	 * @param  int    $user_id The current user ID.
	 * @param  array  $args    Additional arguments for the capability check.
	 */
	return apply_filters( 'cpsf_map_topic_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'bbp_map_topic_meta_caps', 'cpsf_map_topic_caps', 10, 4 );

/**
 * Map reply caps for the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $caps    List of Caps for the current capability check.
 * @param  string $cap     The current capability check.
 * @param  int    $user_id The current user ID.
 * @param  array  $args    Additional arguments for the capability check.
 * @return array           List of Caps for the current capability check.
 */
function cpsf_map_reply_caps( $caps, $cap, $user_id, $args ) {
	if ( ! cp_displayed_site() ) {
		return $caps;
	}

	$is_site_admin = is_super_admin();
	if ( ! $is_site_admin && false !== array_search( $user_id, cp_displayed_site()->admins ) ) {
		$is_site_admin = true;
	}

	switch ( $cap ) {
		case 'publish_replies' :
			if ( ! $is_site_admin && ! empty( $user_id ) ) {
				$user_sites = cp_sites_get_user_sites( $user_id, 'any' );

				// Only site members of followers can publish topics
				if ( false === array_search( cp_get_displayed_site_id(), $user_sites ) ) {
					$caps = array( 'do_not_allow' );
				} else {
					$caps = array( 'exist' );
				}
			}
			break;

		case 'edit_reply'            :
		case 'edit_replies'          :
		case 'edit_others_replies'   :
		case 'delete_replies'        :
		case 'delete_others_replies' :
			if ( $is_site_admin ) {
				$caps = array( 'exist' );
			}
			break;

		case 'moderate'     :
		case 'delete_reply' :
			if ( ! empty( $args[0] ) ) {
				$_reply = get_post( $args[0] );

				if ( (int) bbp_get_reply_forum_id( $_reply->ID ) === (int) cp_displayed_site()->forum_id && $is_site_admin ) {
					$caps = array( 'exist' );
				}
			}
			break;
	}

	/**
	 * Filter here to edit the reply caps map.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $caps    List of Caps for the current capability check.
	 * @param  string $cap     The current capability check.
	 * @param  int    $user_id The current user ID.
	 * @param  array  $args    Additional arguments for the capability check.
	 */
	return apply_filters( 'cpsf_map_reply_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'bbp_map_reply_meta_caps', 'cpsf_map_reply_caps', 10, 4 );

/**
 * Map topic tags caps for the displayed site.
 *
 * @since  1.0.0
 *
 * @param  array  $caps    List of Caps for the current capability check.
 * @param  string $cap     The current capability check.
 * @param  int    $user_id The current user ID.
 * @param  array  $args    Additional arguments for the capability check.
 * @return array           List of Caps for the current capability check.
 */
function cpsf_map_topic_tag_caps( $caps, $cap, $user_id, $args ) {
	if ( ! cp_displayed_site() ) {
		return $caps;
	}

	$is_site_admin = is_super_admin();
	if ( ! $is_site_admin && false !== array_search( $user_id, cp_displayed_site()->admins ) ) {
		$is_site_admin = true;
	}

	switch ( $cap ) {
		case 'manage_topic_tags' :
		case 'edit_topic_tags'   :
		case 'delete_topic_tags' :
			if ( $is_site_admin ) {
				$caps = array( 'exist' );
			}
			break;
	}

	/**
	 * Filter here to edit the topic tags caps map.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $caps    List of Caps for the current capability check.
	 * @param  string $cap     The current capability check.
	 * @param  int    $user_id The current user ID.
	 * @param  array  $args    Additional arguments for the capability check.
	 */
	return apply_filters( 'cpsf_map_topic_tag_caps', $caps, $cap, $user_id, $args );
}
add_filter( 'bbp_map_topic_tag_meta_caps', 'cpsf_map_topic_tag_caps', 10, 4 );

/**
 * Filters the bbPress Topics pagination to adapt it to the displayed site's forum.
 *
 * @since  1.0.0
 *
 * @param  array  $pagination The paginate link arguments built by bbPress.
 * @return array              The paginate link arguments.
 */
function cpsf_site_set_forum_pagination_base( $pagination = array() ) {
	if ( ( ! cpsf_is_single_forum() && ! cpsf_get_topic_tag() ) || ! isset( $pagination['base'] ) ) {
		return $pagination;
	}

	if ( cpsf_get_topic_tag() ) {
		$base  = cpsf_get_topic_tag_link( '', cpsf_get_topic_tag_term(), bbp_get_topic_tag_tax_id() );
	} else {
		$forum = cpsf_get_current_forum();
		$base  = cpsf_get_site_forum_permalink( '', $forum->ID );
	}

	if ( ! $base ) {
		return $pagination;
	}

	if ( ! clusterpress()->permalink_structure ) {
		$pagination['base'] = esc_url_raw( add_query_arg( 'paged', '%#%', $base ) );
	} else {
		$pagination['base'] = trailingslashit( $base ) . cp_get_paged_slug() . '/%#%/';
	}

	return $pagination;
}
add_filter( 'bbp_topic_pagination', 'cpsf_site_set_forum_pagination_base' );

/**
 * Filters the bbPress Replies pagination to adapt it to the displayed site's forum.
 *
 * @since  1.0.0
 *
 * @param  array  $pagination The paginate link arguments built by bbPress.
 * @return array              The paginate link arguments.
 */
function cpsf_site_set_topic_pagination_base( $pagination = array() ) {
	if ( ! cpsf_is_single_topic() || ! isset( $pagination['base'] ) ) {
		return $pagination;
	}

	$topic = cpsf_get_current_topic();
	$base  = cpsf_get_site_topic_permalink( '', $topic->ID );

	if ( ! $base ) {
		return $pagination;
	}

	if ( ! clusterpress()->permalink_structure ) {
		$pagination['base'] = esc_url_raw( add_query_arg( 'paged', '%#%', $base ) );
	} else {
		$pagination['base'] = trailingslashit( $base ) . cp_get_paged_slug() . '/%#%/';
	}

	return $pagination;
}
add_filter( 'bbp_replies_pagination', 'cpsf_site_set_topic_pagination_base' );
