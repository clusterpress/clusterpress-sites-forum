<?php
/**
 * Actions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums
 * @subpackage actions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Registers the displayed user's profile Forum section.
 *
 * @since  1.0.0
 */
function cpsf_register_user_section() {
	$forum_tabs = array(
		array(
			'id'           => 'user_topics',
			'slug'         => cpsf_get_user_topics_archive_slug(),
			'parent'       => 'cp_user_forums',
			'parent_group' => 'single-bar',
			'title'        => __( 'Sujets démarrés', 'clusterpress-sites-forum' ),
			'href'         => '#',
			'position'     => 10,
			'capability'   => 'cp_read_single_user',
		),
		array(
			'id'           => 'user_replies',
			'slug'         => cpsf_get_user_replies_archive_slug(),
			'parent'       => 'cp_user_forums',
			'parent_group' => 'single-bar',
			'title'        => __( 'Réponses apportées', 'clusterpress-sites-forum' ),
			'href'         => '#',
			'position'     => 20,
			'capability'   => 'cp_read_single_user',
		),
	);

	if ( bbp_is_favorites_active() ) {
		$forum_tabs[] = array(
			'id'           => 'user_favorites',
			'slug'         => cpsf_get_user_favorites_archive_slug(),
			'parent'       => 'cp_user_forums',
			'parent_group' => 'single-bar',
			'title'        => __( 'Favoris', 'clusterpress-sites-forum' ),
			'href'         => '#',
			'position'     => 30,
			'capability'   => 'cp_read_single_user',
		);
	}

	if ( bbp_is_subscriptions_active() ) {
		$forum_tabs[] = array(
			'id'           => 'user_subscriptions',
			'slug'         => cpsf_get_user_subscriptions_archive_slug(),
			'parent'       => cp_user_get_manage_rewrite_id(),
			'parent_group' => 'single-bar',
			'title'        => __( 'Forums', 'clusterpress-sites-forum' ),
			'href'         => '#',
			'position'     => 80,
			'capability'   => 'cp_edit_single_user',
		);
	}

	// Registers The User's profile Forum section
	cp_register_cluster_section( 'user', array(
		'id'               => 'sites-forum',
		'name'             => bbp_get_forum_archive_title(),
		'cluster_id'       => 'user',
		'slug'             => cpsf_get_user_forums_archive_slug(),
		'rewrite_id'       => 'cp_user_forums',
		'display_callback' => 'cpsf_display_user_screens',
		'toolbar_items'    => array(
			'position'     => 80,
			'dashicon'     => 'dashicons-megaphone',
			'children'     => $forum_tabs,
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cpsf_register_user_section', 14 );

/**
 * Redirect the bbPress User's profile to the ClusterPress one.
 *
 * @since  1.0.0
 */
function cpsf_redirect_canonical() {
	$redirect_url = '';

	// Viewing a single user
	if ( bbp_is_single_user() ) {
		$user_id = bbp_get_displayed_user_id();

		$redirect_url = cpsf_get_user_profile_url( $user_id );
	}

	/**
	 * Filter here to edit the redirect url.
	 *
	 * @since  1.0.0
	 *
	 * @param string $url The redirect url.
	 */
	$redirect = apply_filters( 'cpsf_redirect_canonical', $redirect_url );

	if ( ! empty( $redirect ) ) {
		wp_redirect( $redirect );
		exit();
	}
}
add_action( 'bbp_template_redirect', 'cpsf_redirect_canonical' );
