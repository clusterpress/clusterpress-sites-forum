<?php
/**
 * Filters
 *
 * @since  1.0.0
 *
 * @package ClusterPress Sites Forum\forums
 * @subpackage filters
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Filters bbPress user profile URL so that it uses the ClusterPress User profile.
 *
 * @since  1.0.0
 *
 * @param  int    $user_id The user ID
 * @return string          The url of the user's ClusterPress profile.
 */
function cpsf_get_user_profile_url( $user_id = 0 ) {
	if ( empty( $user_id ) ) {
		return;
	}

	return cp_user_get_url( array( 'user_id' => $user_id ) );
}
add_filter( 'bbp_pre_get_user_profile_url', 'cpsf_get_user_profile_url', 10, 1 );

/**
 * Filters the bbPress Topics pagination to adapt it to the displayed user's forum section.
 *
 * @since  1.0.0
 *
 * @param  array  $pagination The paginate link arguments built by bbPress.
 * @return array              The paginate link arguments.
 */
function cpsf_user_set_topics_pagination_base( $pagination = array() ) {
	if ( ( ! cpsf_is_user_forum() && ! cpsf_is_manage_user_forum() ) || ! isset( $pagination['base'] ) ) {
		return $pagination;
	}

	$user_topics_slug        = cpsf_get_user_topics_archive_slug();
	$user_favorites_slug     = cpsf_get_user_favorites_archive_slug();
	$user_subscriptions_slug = cpsf_get_user_subscriptions_archive_slug();

	if ( false === array_search( cp_current_sub_action(), array(
		$user_topics_slug,
		$user_favorites_slug,
		$user_subscriptions_slug,
	) ) ) {
		return $pagination;
	}

	$rewrite_id = 'cp_user_forums';

	// Manage Topic subscriptions
	if ( cp_is_current_sub_action( $user_subscriptions_slug ) && cpsf_is_manage_user_forum() ) {
		$slug       = trailingslashit( cp_user_get_manage_slug() ) . $user_subscriptions_slug;
		$rewrite_id = cp_user_get_manage_rewrite_id();

	// List favorites
	} elseif ( cp_is_current_sub_action( $user_favorites_slug ) ) {
		$slug = trailingslashit( cpsf_get_user_forums_archive_slug() ) . $user_favorites_slug;

	// List topics
	} else {
		$slug = trailingslashit( cpsf_get_user_forums_archive_slug() ) . $user_topics_slug;
	}

	$base = cp_user_get_url( array(
		'slug'       => $slug,
		'rewrite_id' => $rewrite_id,
	), cp_displayed_user() );

	if ( ! clusterpress()->permalink_structure ) {
		$pagination['base'] = esc_url_raw( add_query_arg( 'paged', '%#%', $base ) );
	} else {
		$pagination['base'] = trailingslashit( $base ) . cp_get_paged_slug() . '/%#%/';
	}

	return $pagination;
}
add_filter( 'bbp_topic_pagination', 'cpsf_user_set_topics_pagination_base' );

/**
 * Filters the bbPress Replies pagination to adapt it to the displayed user's forum section.
 *
 * @since  1.0.0
 *
 * @param  array  $pagination The paginate link arguments built by bbPress.
 * @return array              The paginate link arguments.
 */
function cpsf_user_set_replies_pagination_base( $pagination = array() ) {
	if ( ! cpsf_is_user_forum() || ! isset( $pagination['base'] ) ) {
		return $pagination;
	}

	$user_replies_slug = cpsf_get_user_replies_archive_slug();

	if ( ! cp_is_current_sub_action( $user_replies_slug ) ) {
		return $pagination;
	}

	$base = cp_user_get_url( array(
		'slug'       => trailingslashit( cpsf_get_user_forums_archive_slug() ) . $user_replies_slug,
		'rewrite_id' => 'cp_user_forums',
	), cp_displayed_user() );

	if ( ! clusterpress()->permalink_structure ) {
		$pagination['base'] = esc_url_raw( add_query_arg( 'paged', '%#%', $base ) );
	} else {
		$pagination['base'] = trailingslashit( $base ) . cp_get_paged_slug() . '/%#%/';
	}

	return $pagination;
}
add_filter( 'bbp_replies_pagination', 'cpsf_user_set_replies_pagination_base' );

/**
 * Deactivate bbPress redirect after login.
 *
 * @since 1.0.0
 *
 * @param  string $url     The URL to redirect the user once logged in.
 * @param  string $raw_url Raw url
 * @param  object $user    User object
 * @return string          The URL to redirect the user once logged in.
 */
function cpsf_redirect_login( $url = '', $raw_url = '', $user = null ) {
	if ( has_filter( 'login_redirect', 'bbp_redirect_login' ) ) {
		remove_filter( 'login_redirect', 'bbp_redirect_login', 2 );
	}

	return $url;
}
add_filter( 'login_redirect', 'cpsf_redirect_login', 0, 3 );
