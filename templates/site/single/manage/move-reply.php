<?php
/**
 * Site Move Reply template.
 *
 * @since  1.0.0
 */
?>

<?php if ( is_user_logged_in() && current_user_can( 'edit_topic', bbp_get_topic_id() ) ) : ?>

	<div id="move-reply-<?php bbp_topic_id(); ?>" class="bbp-reply-move">

		<form id="move_reply" name="move_reply" method="post" action="<?php cpsf_the_reply_move_form_action(); ?>">

			<fieldset class="bbp-form">

				<legend><?php printf( esc_html__( 'Déplacer la réponse "%s"', 'clusterpress-sites-forum' ), bbp_get_reply_title() ); ?></legend>

				<div>

					<div class="bbp-template-notice info">
						<ul>
							<li><?php esc_html_e( 'Vous pouvez faire de cette réponse un nouveau sujet en personnalisant son titre, ou vous pouvez la fusionner avec un des sujets de la liste déroulante.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<div class="bbp-template-notice">
						<ul>
							<li><?php esc_html_e( 'Si vous choisissez un des sujets de la liste déroulante, les réponses seront les réponses seront fusionnées chronologiquement.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<fieldset class="bbp-form">
						<legend><?php esc_html_e( 'Comment déplacer ?', 'clusterpress-sites-forum' ); ?></legend>

						<div>
							<input name="bbp_reply_move_option" id="bbp_reply_move_option_reply" type="radio" checked="checked" value="topic" />
							<label for="bbp_reply_move_option_reply"><?php printf( esc_html__( 'Un nouveau sujet dans %s intitulé :', 'clusterpress-sites-forum' ), bbp_get_forum_title( bbp_get_reply_forum_id( bbp_get_reply_id() ) ) ); ?></label>
							<input type="text" id="bbp_reply_move_destination_title" value="<?php printf( esc_html__( '%s (déplacée)', 'clusterpress-sites-forum' ), bbp_get_reply_title() ); ?>" size="35" name="bbp_reply_move_destination_title" />
						</div>

						<?php if ( cpsf_has_topics_to_move_in() ) : ?>

							<div>
								<input name="bbp_reply_move_option" id="bbp_reply_move_option_existing" type="radio" value="existing" />
								<label for="bbp_reply_move_option_existing"><?php esc_html_e( 'Utiliser un des sujets de la liste déroulante :', 'clusterpress-sites-forum' ); ?></label>

								<?php
									bbp_dropdown( array(
										'post_type'   => bbp_get_topic_post_type(),
										'post_parent' => bbp_get_reply_forum_id( bbp_get_reply_id() ),
										'selected'    => -1,
										'exclude'     => bbp_get_reply_topic_id( bbp_get_reply_id() ),
										'select_id'   => 'bbp_destination_topic'
									) );
								?>

							</div>

						<?php endif; ?>

					</fieldset>

					<?php cpsf_display_feedback( __( 'Attention, il n\'est pas possible de revenir en arrière.', 'clusterpress-sites-forum' ), 'info' ); ?>

					<div class="bbp-submit-wrapper">
						<button type="submit" id="bbp_move_reply_submit" name="bbp_move_reply_submit" class="button submit"><?php esc_html_e( 'Déplacer', 'clusterpress-sites-forum' ); ?></button>
					</div>
				</div>

				<?php bbp_move_reply_form_fields(); ?>

			</fieldset>
		</form>
	</div>

<?php else : ?>

	<div id="no-reply-<?php bbp_reply_id(); ?>" class="bbp-no-reply">
		<div class="entry-content"><?php is_user_logged_in()
			? esc_html_e( 'Vous ne disposez pas des droits suffisants pour réaliser cette opération', 'clusterpress-sites-forum' )
			: esc_html_e( 'Vous ne pouvez pas modifier ce sujet.', 'clusterpress-sites-forum' );
		?></div>
	</div>

<?php endif; ?>
