<?php
/**
 * Site Forum Topic merge template.
 *
 * @since  1.0.0
 */
?>

<?php if ( is_user_logged_in() && current_user_can( 'edit_topic', bbp_get_topic_id() ) ) : ?>

	<div id="merge-topic-<?php bbp_topic_id(); ?>" class="bbp-topic-merge">

		<form id="merge_topic" name="merge_topic" method="post" action="<?php cpsf_the_topic_merge_form_action(); ?>">

			<fieldset class="bbp-form">

				<legend><?php printf( esc_html__( 'Fusionner le sujet "%s"', 'clusterpress-sites-forum' ), bbp_get_topic_title() ); ?></legend>

				<div>

					<div class="bbp-template-notice info">
						<ul>
							<li><?php esc_html_e( 'Sélectionner le sujet avec lequel vous souhaitez fusionner celui-ci. Le sujet de destination restera le sujet principal et celui-ci deviendra une de ses réponses.', 'clusterpress-sites-forum' ); ?></li>
							<li><?php esc_html_e( 'Si vous souhaitez garder ce sujet comme le principal sujet, ouvrez un autre sujet à fusionner avec celui-ci.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<div class="bbp-template-notice">
						<ul>
							<li><?php esc_html_e( 'Les réponses aux deux sujets seront fusionnées chronologiquement.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<fieldset class="bbp-form">
						<legend><?php esc_html_e( 'Destination', 'clusterpress-sites-forum' ); ?></legend>
						<div>
							<?php if ( cpsf_has_topics_to_merge_or_split() ) : ?>

								<label for="bbp_destination_topic"><?php esc_html_e( 'Fusionner le sujet avec :', 'clusterpress-sites-forum' ); ?></label>

								<?php
									bbp_dropdown( array(
										'post_type'   => bbp_get_topic_post_type(),
										'post_parent' => bbp_get_topic_forum_id( bbp_get_topic_id() ),
										'post_status' => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
										'selected'    => -1,
										'exclude'     => bbp_get_topic_id(),
										'select_id'   => 'bbp_destination_topic'
									) );
								?>

							<?php else : ?>

								<label><?php esc_html_e( 'Il n\'y a pas de sujets dans ce forum pour fusionner votre sujet.', 'clusterpress-sites-forum' ); ?></label>

							<?php endif; ?>

						</div>
					</fieldset>

					<fieldset class="bbp-form">
						<legend><?php esc_html_e( 'Options supplémentaires du sujet', 'clusterpress-sites-forum' ); ?></legend>

						<div>

							<?php if ( bbp_is_subscriptions_active() ) : ?>

								<input name="bbp_topic_subscribers" id="bbp_topic_subscribers" type="checkbox" value="1" checked="checked" />
								<label for="bbp_topic_subscribers"><?php esc_html_e( 'Fusionner les abonnés aux sujets', 'clusterpress-sites-forum' ); ?></label><br />

							<?php endif; ?>

							<input name="bbp_topic_favoriters" id="bbp_topic_favoriters" type="checkbox" value="1" checked="checked" />
							<label for="bbp_topic_favoriters"><?php esc_html_e( 'Fusionner les favoris', 'clusterpress-sites-forum' ); ?></label><br />

							<?php if ( bbp_allow_topic_tags() ) : ?>

								<input name="bbp_topic_tags" id="bbp_topic_tags" type="checkbox" value="1" checked="checked" />
								<label for="bbp_topic_tags"><?php esc_html_e( 'Fusionner les étiquettes', 'clusterpress-sites-forum' ); ?></label><br />

							<?php endif; ?>

						</div>
					</fieldset>

					<?php cpsf_display_feedback( __( 'Attention, il n\'est pas possible de revenir en arrière.', 'clusterpress-sites-forum' ), 'info' ); ?>

					<div class="bbp-submit-wrapper">
						<button type="submit" id="bbp_merge_topic_submit" name="bbp_merge_topic_submit" class="button submit"><?php esc_html_e( 'Fusionner', 'clusterpress-sites-forum' ); ?></button>
					</div>
				</div>

				<?php bbp_merge_topic_form_fields(); ?>

			</fieldset>
		</form>
	</div>

<?php else : ?>

	<div id="no-topic-<?php bbp_topic_id(); ?>" class="bbp-no-topic">
		<div class="entry-content"><?php is_user_logged_in()
			? esc_html_e( 'Vous ne disposez pas des droits suffisants pour réaliser cette opération', 'clusterpress-sites-forum' )
			: esc_html_e( 'Vous ne pouvez pas modifier ce sujet.', 'clusterpress-sites-forum' );
		?></div>
	</div>

<?php endif; ?>
