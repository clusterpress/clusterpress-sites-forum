<?php
/**
 * Site Forum Topic merge template.
 *
 * @since  1.0.0
 */
?>

<?php if ( is_user_logged_in() && current_user_can( 'edit_topic', bbp_get_topic_id() ) ) : ?>

	<div id="split-topic-<?php bbp_topic_id(); ?>" class="bbp-topic-split">

		<form id="split_topic" name="split_topic" method="post" action="<?php cpsf_the_topic_split_form_action(); ?>">

			<fieldset class="bbp-form">

				<legend><?php printf( esc_html__( 'Scinder le sujet "%s"', 'clusterpress-sites-forum' ), bbp_get_topic_title() ); ?></legend>

				<div>

					<div class="bbp-template-notice info">
						<ul>
							<li><?php esc_html_e( 'Lorsque vous scindez un sujet, vous le diviser en deux en commençant par la réponse que vous avez sélectionnée. Vous pouvez opter pour utiliser cette réponse comme un nouveau sujet en personnalisant son titre, ou opter pour une fusion des réponses avec le sujet sélectionné dans la liste déroulante.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<div class="bbp-template-notice">
						<ul>
							<li><?php esc_html_e( 'Si vous utilisez un sujet de la liste déroulante, les réponses seront fusionnées chronologiquement.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

					<fieldset class="bbp-form">
						<legend><?php esc_html_e( 'Comment scinder ?', 'clusterpress-sites-forum' ); ?></legend>

						<div>
							<input name="bbp_topic_split_option" id="bbp_topic_split_option_reply" type="radio" checked="checked" value="reply" />
							<label for="bbp_topic_split_option_reply"><?php printf( esc_html__( 'Un nouveau sujet dans %s intitulé :', 'clusterpress-sites-forum' ), bbp_get_forum_title( bbp_get_topic_forum_id( bbp_get_topic_id() ) ) ); ?></label>
							<input type="text" id="bbp_topic_split_destination_title" value="<?php printf( esc_html__( 'Scindé : %s', 'clusterpress-sites-forum' ), bbp_get_topic_title() ); ?>" size="35" name="bbp_topic_split_destination_title" />
						</div>

						<?php if ( cpsf_has_topics_to_merge_or_split() ) : ?>

							<div>
								<input name="bbp_topic_split_option" id="bbp_topic_split_option_existing" type="radio" value="existing" />
								<label for="bbp_topic_split_option_existing"><?php esc_html_e( 'Utiliser un des sujets de la liste déroulante :', 'clusterpress-sites-forum' ); ?></label>

								<?php
									bbp_dropdown( array(
										'post_type'   => bbp_get_topic_post_type(),
										'post_parent' => bbp_get_topic_forum_id( bbp_get_topic_id() ),
										'post_status' => array( bbp_get_public_status_id(), bbp_get_closed_status_id() ),
										'selected'    => -1,
										'exclude'     => bbp_get_topic_id(),
										'select_id'   => 'bbp_destination_topic'
									) );
								?>

							</div>

						<?php endif; ?>

					</fieldset>

					<fieldset class="bbp-form">
						<legend><?php esc_html_e( 'Options supplémentaires du sujet', 'clusterpress-sites-forum' ); ?></legend>

						<div>

							<?php if ( bbp_is_subscriptions_active() ) : ?>

								<input name="bbp_topic_subscribers" id="bbp_topic_subscribers" type="checkbox" value="1" checked="checked" />
								<label for="bbp_topic_subscribers"><?php esc_html_e( 'Reporter les abonnés sur le nouveau sujet', 'clusterpress-sites-forum' ); ?></label><br />

							<?php endif; ?>

							<input name="bbp_topic_favoriters" id="bbp_topic_favoriters" type="checkbox" value="1" checked="checked" />
							<label for="bbp_topic_favoriters"><?php esc_html_e( 'Reporter les favoris sur le nouveau sujet', 'clusterpress-sites-forum' ); ?></label><br />

							<?php if ( bbp_allow_topic_tags() ) : ?>

								<input name="bbp_topic_tags" id="bbp_topic_tags" type="checkbox" value="1" checked="checked" />
								<label for="bbp_topic_tags"><?php esc_html_e( 'Reporter les étiquettes sur le nouveau sujet', 'clusterpress-sites-forum' ); ?></label><br />

							<?php endif; ?>

						</div>
					</fieldset>

					<?php cpsf_display_feedback( __( 'Attention, il n\'est pas possible de revenir en arrière.', 'clusterpress-sites-forum' ), 'info' ); ?>

					<div class="bbp-submit-wrapper">
						<button type="submit" id="bbp_merge_topic_submit" name="bbp_merge_topic_submit" class="button submit"><?php esc_html_e( 'Scinder', 'clusterpress-sites-forum' ); ?></button>
					</div>
				</div>

				<?php bbp_split_topic_form_fields(); ?>

			</fieldset>
		</form>
	</div>

<?php else : ?>

	<div id="no-topic-<?php bbp_topic_id(); ?>" class="bbp-no-topic">
		<div class="entry-content"><?php is_user_logged_in()
			? esc_html_e( 'Vous ne disposez pas des droits suffisants pour réaliser cette opération', 'clusterpress-sites-forum' )
			: esc_html_e( 'Vous ne pouvez pas modifier ce sujet.', 'clusterpress-sites-forum' );
		?></div>
	</div>

<?php endif; ?>
