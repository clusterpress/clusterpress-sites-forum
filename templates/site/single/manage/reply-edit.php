<?php
/**
 * Site Forum Reply edit template.
 *
 * @since  1.0.0
 */
?>

<?php cpsf_set_forum_object( 'reply' ); ?>

<div id="bbpress-forums">

	<?php if ( bbp_is_reply_move() ) :

		cp_get_template_part( 'site/single/manage/move-reply' );

	else :

		if ( bbp_current_user_can_access_create_reply_form() ) : ?>

		<div id="new-reply-<?php bbp_topic_id(); ?>" class="bbp-reply-form">

			<form id="new-post" name="new-post" method="post" action="<?php cpsf_the_reply_edit_form_action(); ?>">

				<fieldset class="bbp-form">
					<legend><?php printf( esc_html__( 'En réponse à : %s', 'clusterpress-sites-forum' ), ( bbp_get_form_reply_to() ) ? sprintf( esc_html__( 'Réponse #%1$s dans %2$s', 'clusterpress-sites-forum' ), bbp_get_form_reply_to(), bbp_get_topic_title() ) : bbp_get_topic_title() ); ?></legend>

					<?php if ( ! bbp_is_topic_open() && ! bbp_is_reply_edit() ) : ?>

						<div class="bbp-template-notice">
							<ul>
								<li><?php esc_html_e( 'Le topic est fermé, toutefois vos droits vous permettent de continuer à y répondre.', 'clusterpress-sites-forum' ); ?></li>
							</ul>
						</div>

					<?php endif; ?>

					<?php if ( current_user_can( 'unfiltered_html' ) ) : ?>

						<div class="bbp-template-notice">
							<ul>
								<li><?php esc_html_e( 'Vos droits vous permettent de poster sans restriction de balises HTML.', 'clusterpress-sites-forum' ); ?></li>
							</ul>
						</div>

					<?php endif; ?>

					<?php do_action( 'bbp_template_notices' ); ?>

					<div>

						<?php bbp_the_content( array( 'context' => 'reply' ) ); ?>

						<?php if ( ! ( bbp_use_wp_editor() || current_user_can( 'unfiltered_html' ) ) ) : ?>

							<p class="form-allowed-tags">
								<label><?php esc_html_e( 'Vous pouvez utiliser ces balises <abbr title="HyperText Markup Language">HTML</abbr> :','clusterpress-sites-forum' ); ?></label><br />
								<code><?php bbp_allowed_tags(); ?></code>
							</p>

						<?php endif; ?>

						<?php if ( bbp_allow_topic_tags() && current_user_can( 'assign_topic_tags' ) ) : ?>

							<p>
								<label for="bbp_topic_tags"><?php esc_html_e( 'Etiquettes :', 'clusterpress-sites-forum' ); ?></label><br />
								<input type="text" value="<?php bbp_form_topic_tags(); ?>" size="40" name="bbp_topic_tags" id="bbp_topic_tags" <?php disabled( bbp_is_topic_spam() ); ?> />
							</p>

						<?php endif; ?>

						<?php if ( bbp_is_subscriptions_active() && ! bbp_is_anonymous() ) : ?>

							<p>

								<input name="bbp_topic_subscription" id="bbp_topic_subscription" type="checkbox" value="bbp_subscribe"<?php bbp_form_topic_subscribed(); ?> />

								<?php if ( bbp_is_reply_edit() && ( bbp_get_reply_author_id() !== bbp_get_current_user_id() ) ) : ?>

									<label for="bbp_topic_subscription"><?php esc_html_e( 'Tenir informé l\'auteur des prochaines réponses à ce sujet', 'clusterpress-sites-forum' ); ?></label>

								<?php else : ?>

									<label for="bbp_topic_subscription"><?php esc_html_e( 'M\'informer des prochaines réponses à ce sujet', 'clusterpress-sites-forum' ); ?></label>

								<?php endif; ?>

							</p>

						<?php endif; ?>

						<?php if ( bbp_is_reply_edit() ) : ?>

							<?php if ( bbp_allow_revisions() ) : ?>

								<fieldset class="bbp-form">
									<legend>
										<input name="bbp_log_reply_edit" id="bbp_log_reply_edit" type="checkbox" value="1" <?php bbp_form_reply_log_edit(); ?> />
										<label for="bbp_log_reply_edit"><?php esc_html_e( 'Journaliser cette modification :', 'clusterpress-sites-forum' ); ?></label><br />
									</legend>

									<div>
										<label for="bbp_reply_edit_reason"><?php printf( esc_html__( 'Raison de la modification (optionel):', 'clusterpress-sites-forum' ), bbp_get_current_user_name() ); ?></label><br />
										<input type="text" value="<?php bbp_form_reply_edit_reason(); ?>" size="40" name="bbp_reply_edit_reason" id="bbp_reply_edit_reason" />
									</div>
								</fieldset>

							<?php endif; ?>

						<?php endif; ?>

						<div class="bbp-submit-wrapper">

							<?php bbp_cancel_reply_to_link(); ?>

							<button type="submit" id="bbp_reply_submit" name="bbp_reply_submit" class="button submit"><?php esc_html_e( 'Sauvegarder', 'clusterpress-sites-forum' ); ?></button>

						</div>

					</div>

					<?php bbp_reply_form_fields(); ?>

				</fieldset>

			</form>
		</div>

	<?php elseif ( bbp_is_topic_closed() ) : ?>

		<div id="no-reply-<?php bbp_topic_id(); ?>" class="bbp-no-reply">
			<div class="bbp-template-notice">
				<ul>
					<li><?php printf( esc_html__( 'Le sujet &#8216;%s&#8217; est fermé aux nouvelles réponses.', 'clusterpress-sites-forum' ), bbp_get_topic_title() ); ?></li>
				</ul>
			</div>
		</div>

	<?php elseif ( bbp_is_forum_closed( bbp_get_topic_forum_id() ) ) : ?>

		<div id="no-reply-<?php bbp_topic_id(); ?>" class="bbp-no-reply">
			<div class="bbp-template-notice">
				<ul>
					<li><?php printf( esc_html__( 'Le forum &#8216;%s&#8217; est fermé aux nouveaux sujets et réponses.', 'clusterpress-sites-forum' ), bbp_get_forum_title( bbp_get_topic_forum_id() ) ); ?></li>
				</ul>
			</div>
		</div>

	<?php endif;

endif; ?>

</div>

<?php cpsf_reset_forum_object( 'reply' ); ?>
