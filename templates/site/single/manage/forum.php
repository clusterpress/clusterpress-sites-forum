<?php
/**
 * Site Forum admin template.
 *
 * This template chooses the best template part to load
 * according to the context.
 *
 * @since  1.0.0
 */
?>
<div id="bbpress-forums">

	<?php cpsf_template_notices(); ?>

	<form action="" method="post" class="cp-form">

		<?php cpsf_set_forum_object(); ?>

		<fieldset class="bbp-form">
			<legend>

				<?php if ( cpsf_is_forum_confirm_delete() ) :

					esc_html_e( 'Confirmer la suppression du forum du site', 'clusterpress-sites-forum' );

				elseif ( bbp_is_forum_edit() ) :

					esc_html_e( 'Modifier le forum du site', 'clusterpress-sites-forum' );

				else :

					esc_html_e( 'Créer le forum du site', 'clusterpress-sites-forum' );

				endif; ?>

			</legend>

			<div>

				<input type="hidden" name="bbp_forum_parent_id" value="<?php cpsf_the_forum_parent_id(); ?>">
				<input type="hidden" name="bbp_forum_title" value="<?php cpsf_the_forum_title(); ?>">
				<input type="hidden" name="redirect_to" value="<?php cpsf_the_redirect_url(); ?>">

				<?php if ( cpsf_is_forum_confirm_delete() ) : ?>

					<?php cpsf_display_feedback( __( 'Attention, il n\'est pas possible de revenir en arrière. Le forum du site ainsi que ses sujets et réponses seront mis à la corbeille et le lien entre le forum et ce site sera définitivement supprimé.', 'clusterpress-sites-forum' ), 'info' ); ?>

					<div>
						<label for="site-forum-delete-cb">
							<input type="checkbox" value="1" id="site-forum-delete-cb" name="cp_sites_forum[confirm]">
							<?php esc_html_e( 'Merci de confirmer votre souhait de supprimer ce forum du site', 'clusterpress-sites-forum' ); ?>
						</label>
					</div>

					<div class="bbp-submit-wrapper">
						<?php wp_nonce_field( 'cp-sites-forum-delete' ); ?>

						<button type="submit" name="cp_sites_forum[trash]" class="button submit"><?php esc_html_e( 'Supprimer', 'clusterpress-sites-forum' ); ?></button>

					</div>

				<?php else : ?>

					<?php bbp_the_content( array( 'context' => 'forum' ) ); ?>

					<div class="bbp-submit-wrapper">

						<?php if ( bbp_is_forum_edit() ) : ?>
							<button class="button submit" id="site-forum-delete">
								<a href="<?php cpsf_forum_confirm_delete_link(); ?>"><?php esc_html_e( 'Supprimer', 'clusterpress-sites-forum' ); ?></a>
							</button>
						<?php endif ; ?>

						<button type="submit" id="bbp_forum_submit" name="bbp_forum_submit" class="button submit"><?php esc_html_e( 'Sauvegarder', 'clusterpress-sites-forum' ); ?></button>

					</div>

				<?php endif; ?>

			</div>

			<?php if ( ! cpsf_is_forum_confirm_delete() ) bbp_forum_form_fields(); ?>

		</fieldset>

		<?php cpsf_reset_forum_object(); ?>

	</form>

</div>
