<?php
/**
 * Site Forum Topic edit template.
 *
 * @since  1.0.0
 */
?>

<div id="bbpress-forums">

	<?php cpsf_set_forum_queried_object(); ?>

	<?php bbp_get_template_part( 'form', 'topic-tag' ); ?>

	<?php cpsf_reset_forum_queried_object(); ?>

</div>
