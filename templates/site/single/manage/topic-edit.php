<?php
/**
 * Site Forum Topic edit template.
 *
 * @since  1.0.0
 */
?>

<div class="site-forum-header-links">
	<?php bbp_topic_tag_list( bbp_get_topic_id(), array(
		'before' => '<div class="site-forum-topic-tags"><span class="dashicons dashicons-tag"></span>',
		'after'  => '</div>',
	) ); ?>

</div>

<?php cpsf_display_feedback( bbp_get_single_topic_description( array(
	'before'   => '',
	'after'    => '',
	'topic_id' => bbp_get_topic_id(),
) ), 'info' ); ?>

<?php if ( bbp_current_user_can_access_create_topic_form() ) : ?>

	<div id="new-topic-<?php bbp_topic_id(); ?>" class="bbp-topic-form">

		<form id="new-post" name="new-post" method="post" action="<?php cpsf_the_topic_edit_form_action(); ?>">

			<fieldset class="bbp-form">
				<legend>

					<?php printf( esc_html__( 'Edition en cours de &ldquo;%s&rdquo;', 'clusterpress-sites-forum' ), bbp_get_topic_title() ); ?>

				</legend>

				<?php if ( current_user_can( 'unfiltered_html' ) ) : ?>

					<div class="bbp-template-notice">
						<ul>
							<li><?php esc_html_e( 'Vos droits vous permettent de poster sans restriction de balises HTML.', 'clusterpress-sites-forum' ); ?></li>
						</ul>
					</div>

				<?php endif; ?>

				<?php do_action( 'bbp_template_notices' ); ?>

				<div>

					<p>
						<label for="bbp_topic_title"><?php printf( esc_html__( 'Titre du sujet (Nombre de caractères max. : %d):', 'clusterpress-sites-forum' ), bbp_get_title_max_length() ); ?></label><br />
						<input type="text" id="bbp_topic_title" value="<?php bbp_form_topic_title(); ?>" size="40" name="bbp_topic_title" maxlength="<?php bbp_title_max_length(); ?>" />
					</p>

					<?php bbp_the_content( array( 'context' => 'topic' ) ); ?>

					<?php if ( ! ( bbp_use_wp_editor() || current_user_can( 'unfiltered_html' ) ) ) : ?>

						<p class="form-allowed-tags">
							<label><?php printf( esc_html__( 'Vous pouvez utiliser ces balises %s :', 'clusterpress-sites-forum' ), '<abbr title="HyperText Markup Language">HTML</abbr>' ); ?></label><br />
							<code><?php bbp_allowed_tags(); ?></code>
						</p>

					<?php endif; ?>

					<?php if ( bbp_allow_topic_tags() && current_user_can( 'assign_topic_tags' ) ) : ?>

						<p>
							<label for="bbp_topic_tags"><?php esc_html_e( 'Etiquettes :', 'clusterpress-sites-forum' ); ?></label><br />
							<input type="text" value="<?php bbp_form_topic_tags(); ?>" size="40" name="bbp_topic_tags" id="bbp_topic_tags" <?php disabled( bbp_is_topic_spam() ); ?> />
						</p>

					<?php endif; ?>

					<?php if ( current_user_can( 'manage_sites' ) ) : ?>

						<p>
							<label for="bbp_forum_id"><?php esc_html_e( 'Forum :', 'clusterpress-sites-forum' ); ?></label><br />
							<?php
								bbp_dropdown( array(
									'show_none' => esc_html__( '&mdash; Aucun forum &mdash;', 'clusterpress-sites-forum' ),
									'selected'  => bbp_get_form_topic_forum()
								) );
							?>
						</p>

					<?php else : ?>

						<input type="hidden" name="bbp_forum_id" value="<?php echo esc_attr( bbp_get_topic_forum_id() ); ?>"

					<?php endif; ?>

					<?php if ( current_user_can( 'moderate', bbp_get_topic_id() ) ) : ?>

						<p>

							<label for="bbp_stick_topic"><?php esc_html_e( 'Type de sujet :', 'clusterpress-sites-forum' ); ?></label><br />

							<?php bbp_form_topic_type_dropdown(); ?>

						</p>

						<p>

							<label for="bbp_topic_status"><?php esc_html_e( 'Etat du sujet :', 'clusterpress-sites-forum' ); ?></label><br />

							<?php bbp_form_topic_status_dropdown(); ?>

						</p>

					<?php endif; ?>

					<?php if ( bbp_is_subscriptions_active() && ! bbp_is_anonymous() ) : ?>

						<p>
							<input name="bbp_topic_subscription" id="bbp_topic_subscription" type="checkbox" value="bbp_subscribe" <?php bbp_form_topic_subscribed(); ?> />

							<?php if ( bbp_is_topic_edit() && ( bbp_get_topic_author_id() !== bbp_get_current_user_id() ) ) : ?>

								<label for="bbp_topic_subscription"><?php esc_html_e( 'Tenir informé l\'auteur des prochaines réponses à ce sujet', 'clusterpress-sites-forum' ); ?></label>

							<?php else : ?>

								<label for="bbp_topic_subscription"><?php esc_html_e( 'M\'informer des prochaines réponses à ce sujet', 'clusterpress-sites-forum' ); ?></label>

							<?php endif; ?>
						</p>

					<?php endif; ?>

					<?php if ( bbp_allow_revisions() ) : ?>

						<fieldset class="bbp-form">
							<legend>
								<input name="bbp_log_topic_edit" id="bbp_log_topic_edit" type="checkbox" value="1" <?php bbp_form_topic_log_edit(); ?> />
								<label for="bbp_log_topic_edit"><?php esc_html_e( 'Journaliser cette modification :', 'clusterpress-sites-forum' ); ?></label><br />
							</legend>

							<div>
								<label for="bbp_topic_edit_reason"><?php printf( esc_html__( 'Raison de la modification (optionel):', 'clusterpress-sites-forum' ), bbp_get_current_user_name() ); ?></label><br />
								<input type="text" value="<?php bbp_form_topic_edit_reason(); ?>" size="40" name="bbp_topic_edit_reason" id="bbp_topic_edit_reason" />
							</div>
						</fieldset>

					<?php endif; ?>

					<div class="bbp-submit-wrapper">

						<input type="hidden" name="redirect_to" value="<?php cpsf_the_topic_redirect_url( bbp_get_topic_id() ); ?>">
						<button type="submit" id="bbp_topic_submit" name="bbp_topic_submit" class="button submit"><?php esc_html_e( 'Sauvegarder', 'clusterpress-sites-forum' ); ?></button>

					</div>

				</div>

				<?php bbp_topic_form_fields(); ?>

			</fieldset>

		</form>
	</div>

<?php elseif ( bbp_is_forum_closed() ) : ?>

	<div id="forum-closed-<?php bbp_forum_id(); ?>" class="bbp-forum-closed">
		<div class="bbp-template-notice">
			<ul>
				<li><?php printf( esc_html__( 'Le forum &#8216;%s&#8217; est fermé aux nouveaux sujets et réponses.', 'clusterpress-sites-forum' ), bbp_get_forum_title() ); ?></li>
			</ul>
		</div>
	</div>

<?php endif; ?>
