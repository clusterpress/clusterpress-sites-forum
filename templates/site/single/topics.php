<?php
/**
 * Site Forum Topics archive template.
 *
 * @since  1.0.0
 */
?>

<?php cpsf_set_forum_object(); ?>

<div class="site-forum-header-links">

	<?php cpsf_site_forum_subscription_link(); ?>

</div>

<?php if ( cpsf_get_topic_tag() ) : ?>

	<div class="topic-tag-description">

		<h3><?php printf( esc_html__( 'Archive des sujets pour l\'étiquette : %s', 'clusterpress-sites-forum' ), bbp_get_topic_tag_name() ); ?></h3>

		<?php cpsf_the_topic_tag_description() ; ?>
	</div>

<?php else : ?>

	<div class="site-forum-intro">

		<?php cpsf_the_site_single_forum_intro(); ?>

	</div>

<?php endif; ?>

<div id="bbpress-forums">

	<?php if ( bbp_get_forum_topic_count() ) : ?>

		<?php cpsf_display_feedback( bbp_get_single_forum_description( array(
			'before' => '',
			'after'  => '',
		) ), 'info' ); ?>

		<form action="" method="get" class="cp-form">
			<div class="cp-search-area cp-form-section">
				<label for="cp-form-search" class="screen-reader-text"><?php cp_search_placehoder(); ?></label>
				<input type="search" placeholder="<?php cp_search_placehoder(); ?>" name="ts" id="cp-form-search" value="<?php echo esc_attr( cpsf_get_search_terms() ); ?>">

				<button type="submit">
					<span class="dashicons dashicons-search"></span>
					<span class="screen-reader-text"><?php cp_search_placehoder(); ?></span>
				</button>
			</div>
		</form>

	<?php endif ; ?>

	<?php if ( cpsf_site_has_topics() ) : ?>

		<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

		<?php bbp_get_template_part( 'loop',       'topics'    ); ?>

		<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

		<?php if ( ! cpsf_get_topic_tag() ) cpsf_site_single_forum_new_topic(); ?>

	<?php else : ?>

		<?php if ( ! cpsf_is_site_forum_search() ) : ?>

			<?php cpsf_display_feedback( __( 'Aucun sujet n\'a été publié jusqu\'à présent, utilisez le forumulaire ci-dessous pour publier le premier', 'clusterpress-sites-forum' ), 'forum[no-result]', 'info' ); ?>

		<?php else : ?>

			<?php cpsf_display_feedback( __( 'Aucun sujet ne correspond à vos critères de recherche.', 'clusterpress-sites-forum' ), 'forum[no-search-result]', 'info' ); ?>

		<?php endif ; ?>

		<?php cpsf_site_single_forum_new_topic(); ?>

	<?php endif; ?>

</div>

<?php cpsf_reset_forum_object(); ?>
