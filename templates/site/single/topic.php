<?php
/**
 * Site Forum Topic single template.
 *
 * @since  1.0.0
 */
?>

<?php cpsf_set_forum_object( 'topic' ); ?>

<div id="bbpress-forums">

	<?php if ( bbp_is_topic_merge() ) :

		cp_get_template_part( 'site/single/manage/topic-merge' );

	elseif ( bbp_is_topic_split() ) :

		cp_get_template_part( 'site/single/manage/topic-split' );

	elseif ( cpsf_is_topic_edit() ) :

		cp_get_template_part( 'site/single/manage/topic-edit' );

	else : ?>

		<h3><?php bbp_topic_title(); ?></h3>

		<div class="site-forum-header-links">
			<?php bbp_topic_tag_list( bbp_get_topic_id(), array(
				'before' => '<div class="site-forum-topic-tags"><span class="dashicons dashicons-tag"></span>',
				'after'  => '</div>',
			) ); ?>

			<?php cpsf_site_topic_subscription_link(); ?>

			<?php cpsf_site_topic_favorites_link(); ?>

		</div>

		<?php cpsf_display_feedback( bbp_get_single_topic_description( array(
			'before' => '',
			'after'  => '',
		) ), 'info' ); ?>

		<?php if ( bbp_show_lead_topic() ) : ?>

			<?php bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

		<?php endif; ?>

			<?php if ( cpsf_topic_has_replies() ) : ?>

				<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

				<?php cpsf_site_single_topic_loop_replies(); ?>

				<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

			<?php endif; ?>

		<?php cpsf_site_single_topic_new_reply() ; ?>

	<?php endif; ?>

</div>

<?php cpsf_reset_forum_object( 'topic' ); ?>
