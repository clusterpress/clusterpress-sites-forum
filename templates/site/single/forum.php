<?php
/**
 * Site Forum root template.
 *
 * This template chooses the best template part to load
 * according to the context.
 *
 * @since  1.0.0
 */
?>

<?php if ( ! cp_is_site_manage() ) : ?>

	<h2><?php cpsf_site_forum_title(); ?></h2>

	<?php if ( cpsf_is_single_forum() ) :

		cp_get_template_part( 'site/single/topics' );

	elseif ( cpsf_is_topic_tag_edit() ) :

		cp_get_template_part( 'site/single/manage/tag-edit' );

	elseif ( cpsf_is_single_topic() ) :

		cp_get_template_part( 'site/single/topic' );

	elseif ( cpsf_is_reply_edit() ) :

		cp_get_template_part( 'site/single/manage/reply-edit' );

	endif; ?>

<?php else : ?>

	<?php cp_get_template_part( 'site/single/manage/forum' ); ?>

<?php endif; ?>
